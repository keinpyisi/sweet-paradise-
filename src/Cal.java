
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JCalendar;
import com.toedter.components.JSpinField;
import com.toedter.calendar.JYearChooser;
import com.toedter.calendar.demo.DateChooserPanel;
import com.toedter.components.JLocaleChooser;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import com.toedter.calendar.JDateChooser;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Cal extends JFrame {
	private JScrollPane scrollPane;
	private JButton add,increase,choose;
	private JDateChooser dateChooser;
	static DefaultTableModel dtm;
        static int q;
        private JComboBox typespinner,itemname;
        private JTextField nametxt,pricetxt;
        private JLabel totel;
	JTable tbndisplay;
        File files;
        private JLabel imagelabel;
        boolean fil = false;
JFileChooser chooser = new JFileChooser();
	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public Cal() {
            try {
                 URL iconurl= getClass().getResource("img/icon.PNG");
              Toolkit tk= this.getToolkit();
               Image ima= tk.getImage(iconurl);
               setIconImage(ima);
                setTitle("Calendar");
                setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                setBounds(100, 100, 849, 631);
                getContentPane().setLayout(null);
                BufferedImage myImage = ImageIO.read(getClass().getResourceAsStream("img/Inventory.jpg"));
                ImageIcon icon= new ImageIcon(myImage);
                JLabel background= new JLabel(icon);
                background.setBounds(0, 0, 849, 631);
                JLabel lblNewLabel = new JLabel("Sweet Paradise");
                lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
                lblNewLabel.setForeground(Color.WHITE);
                lblNewLabel.setFont(new Font("Time New Roman", Font.BOLD, 25));
                lblNewLabel.setBounds(27, 13, 294, 60);
                background.add(lblNewLabel);
                
                JLabel lblNewLabel_1 = new JLabel("Bakery & Cafe");
                lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
                lblNewLabel_1.setForeground(Color.WHITE);
                lblNewLabel_1.setBounds(27, 66, 275, 39);
                lblNewLabel_1.setFont(new Font("Time New Roman", Font.BOLD, 25));
                background.add(lblNewLabel_1);
                this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
         //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowClosing(WindowEvent e) {
                
            }

            @Override
            public void windowClosed(WindowEvent e) {
                try {
                   setVisible(false);
                    JFrame ma=new Main();
                    //setVisible(false);
//           SwingUtilities.getWindowAncestor(this).setVisible(false);
java.awt.EventQueue.invokeLater(new Runnable() {
    public void run() {
        ma.setVisible(true);
    }
});     } catch (Exception ex) {
                   
                } 
            }

            @Override
            public void windowIconified(WindowEvent e) {
          //      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
       //         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowActivated(WindowEvent e) {
       //         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
       //         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
                typespinner=new JComboBox();
                typespinner.setBounds(410, 36, 175, 29);
                ResultSet rs= BakeryController.typename();
                while(rs.next())
                {
                    String names= rs.getString(1);
                    typespinner.addItem(names);
                }
                choose = new JButton("Choose Image");
                choose.setBounds(410, 96, 175, 29);
                choose.addActionListener(new ActionListener() {
                     @Override
                     public void actionPerformed(ActionEvent e) {
                      //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                      int retrival = chooser.showSaveDialog(null);
                if (retrival == JFileChooser.APPROVE_OPTION) {
        try {
            files = chooser.getSelectedFile();
            JOptionPane.showMessageDialog(null, "Image Added");
             BufferedImage im = ImageIO.read(files);
             im =Receipt.linearResizeBi(im, 50 , 50);
             ImageIcon mats= new ImageIcon(im);
            
                imagelabel = new JLabel(mats);
                imagelabel.setBounds(410, 156, 50, 50);
                background.add(imagelabel);
          fil = true;
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
                     }
                 });
                background.add(choose);
               
//                 itemname=new JComboBox();
//                itemname.setBounds(410, 96, 175, 29);
//                  ResultSet rss= BakeryController.itemstatement();
//                while(rss.next())
//                {
//                    String namess= rss.getString(1);
//                    itemname.addItem(namess);
//                }
//                background.add(itemname);
               
                background.add(typespinner);
               //  SpinnerModel sm = new SpinnerNumberModel(0, 0, 100, 5); //default value,lower bound,upper bound,increment by
   nametxt = new JTextField("Name:");
               
               // quantity= new JComboBox();
               // quantity.setOpaque(!isOpaque());
               // quantity.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
                nametxt.setBounds(630, 36, 175, 29);
               
                background.add(nametxt);
                 pricetxt = new JTextField("Price:");
                 
                pricetxt.setBounds(630, 96, 175, 29);
                background.add(pricetxt);

                
//                SpinnerModel sms = new SpinnerNumberModel(0, 0, 100, 5); //default value,lower bound,upper bound,increment by
//   JSpinner spinners = new JSpinner(sms);
//     spinners.setBounds(630, 96, 175, 29);
//                ((DefaultEditor) spinners.getEditor()).getTextField().setEditable(false);
//                background.add(spinners);
                
                increase=new JButton("Add Menu");
                increase.setBounds(630, 156, 175, 39);
                increase.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                 try {
                            //        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                          if(fil == false){
                              JOptionPane.showMessageDialog(null, "Please Choose Image");
                          }else if(fil == true){
                               String names= nametxt.getText().toString();
                            String type= (String)typespinner.getSelectedItem();
                            String arrtibute = " ";
                            String price = pricetxt.getText().toString();
                            if(type.toLowerCase().contains("hot")){
                                arrtibute = "hot";
                            }
                            if(type.toLowerCase().contains("cold")){
                                arrtibute = "cold";
                            }
                             
                           // File file= new File("your_path");
                           
FileInputStream inputStream= new FileInputStream(files);
                     BakeryController.addmenuitem(names, type, price, arrtibute, inputStream);
                           
//                            ingredientpanel.refresh();
//                            InventoryPanel.refresh();
//                            inventoryview.redraw();
                            JOptionPane.showMessageDialog(null, " Added Menu Quantity");
                            
                          }
                           
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                            ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(Cal.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(Cal.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    
                    }
                });
                
                background.add(increase);
                
                
                //add.setBounds(410, 156, 175, 39);
                
                JSeparator separator_1 = new JSeparator();
                separator_1.setBounds(0, 200, 831, 2);
                background.add(separator_1);
                
                totel=new JLabel("");
                totel.setBounds(27, 453, 100 , 100);
                //totel.setBackground(Color.red);
                totel.setVisible(false);
                background.add(totel);
                
                JButton btnNewButton = new JButton("Back");
                btnNewButton.setBounds(40, 516, 128, 39);
                btnNewButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            //     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            new inventoryview();
                            setVisible(false);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                           ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
                        } catch (ClassNotFoundException ex) {
                           
                        } catch (IOException ex) {
                           
                        } catch (FontFormatException ex) {
                           
                        }
                    }
                });
             //   background.add(btnNewButton);
                
                JSeparator separator_2 = new JSeparator();
                separator_2.setOrientation(SwingConstants.VERTICAL);
                separator_2.setBounds(398, 0, 2, 202);
                background.add(separator_2);
                
                
                String col[]= {"Sale Date","Name","Quantity", "Price","Customer ID"};
                String data[][]= {};
                
                dtm=new DefaultTableModel(data,col);
                
                tbndisplay=new JTable(dtm);
                tbndisplay.getColumn(col[0]).setPreferredWidth(150);
                tbndisplay.getColumn(col[1]).setPreferredWidth(200);
                tbndisplay.getColumn(col[2]).setPreferredWidth(150);
                tbndisplay.getColumn(col[4]).setPreferredWidth(450);
                tbndisplay.setOpaque(false);
                //   jt.setOpaque(true);
            
               
                // jt.setBackground(Color.red);
                tbndisplay.setShowGrid(false);
                
                scrollPane = new JScrollPane(tbndisplay,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
                scrollPane.setBounds(23, 213, 549, 279);
                scrollPane.getViewport().setBackground(new
        Color(0,0,0,0));
                scrollPane.setOpaque(!isOpaque());
                scrollPane.getViewport().setOpaque(!isOpaque());
                
                background.add(scrollPane);
                
                dateChooser = new JDateChooser();
                
                dateChooser.setDateFormatString("yyyy-MM-dd");
                dateChooser.setBounds(581, 225, 220, 31);
                background.add(dateChooser);
                
                JButton btnNewButton_1 = new JButton("View");
                btnNewButton_1.setBounds(590, 305, 97, 25);
               // ImageIcon icons= new ImageIcon(getClass().getResource("img/logo.png"));
               // JLabel logo= new JLabel(icons);
              //  logo.setBounds(630, 305, 220, 220);
              //  getContentPane().add(logo);
                getContentPane().add(btnNewButton_1);
                btnNewButton_1.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        dtm.getDataVector().clear();
                        haha1();
                        
                    }
                });
                
                
                
                               
                
                
                
                getContentPane().add(background);
                
                
                setVisible(true);
            } catch (IOException ex) {
               
            } catch (SQLException ex) {
                ex.printStackTrace();
               ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
            } catch (ClassNotFoundException ex) {
               
            }
	
	
	
		
		

	}
  public static int Total() {
		q=0;
              
		for(int i=0;i<dtm.getRowCount();i++) {
			                
				q=q+Integer.parseInt((String)dtm.getValueAt(i, 3)) ;
				
				
			}
             
		return q;
		
		}
public static void main(String[] args) {
            try {
                UIManager.setLookAndFeel("de.javasoft.plaf.synthetica.SyntheticaBlackMoonLookAndFeel");
                new Cal();
            } catch (ClassNotFoundException ex) {
               
            } catch (InstantiationException ex) {
               
            } catch (IllegalAccessException ex) {
               
            } catch (UnsupportedLookAndFeelException ex) {
               
            }
	
}

public void haha1() {
	
	String sql = "select * from sale where saledate=?";
	SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
	String gg=df.format(dateChooser.getDate());
        
       
	
	try {
		
		Connection con = BakeryController.databaseconnection();
		
		PreparedStatement psmt = con.prepareStatement(sql);
		psmt.setString(1, gg);
		ResultSet rs = psmt.executeQuery();
		
		int colCount = rs.getMetaData().getColumnCount();
		while (rs.next()) {//if more row left
			String oneRow[] = new String[colCount];
                         oneRow[0]=rs.getString("saledate").toString();
                         oneRow[1]=rs.getString("name");
                         oneRow[2]=rs.getString("quantity");
                         oneRow[3]=rs.getString("Price");
                         oneRow[4]=rs.getString("UID");
                         //for (int i = 1; i < oneRow.length; i++) {
                         //   System.out.println(rs.getString(i+1));
		//		oneRow[i] = rs.getString(i+1);
				
		//	}
			dtm.addRow(oneRow);
			tbndisplay.repaint();
		}
                
		rs.close();
		psmt.close();
		con.close();
                totel.setText("Total On This Day: " +Total() + "");
                totel.setVisible(true);
              //   Total();
	} catch (Exception e) {
            e.printStackTrace();
		
	}
}

}