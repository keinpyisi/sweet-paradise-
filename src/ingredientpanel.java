
import com.mysql.jdbc.Statement;
import java.awt.Color;
import java.awt.FontFormatException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krul
 */
public class ingredientpanel extends JPanel{

    /**
     * @param args the command line arguments
     */
    private static DefaultTableModel dtm;
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            new ingredientpanel();
        } catch (ClassNotFoundException ex) {
            
        } catch (SQLException ex) {
           ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
        } catch (IOException ex) {
            
        } catch (FontFormatException ex) {
            
        }
    }
    public ingredientpanel() throws ClassNotFoundException, SQLException, IOException, FontFormatException{
        setSize(350,400);
        setLayout(null);
               String[] al= null;
               
  //             System.out.println("RS"+ rs.last());
  ResultSet rs= BakeryController.inventorystatement();
               int count=0;
               //int i=0;
               ArrayList<String[]> als= new ArrayList<>();
               while(rs.next())
               {
                //   al=new ArrayList(rs.getString("AtdDate"), rs.getString("attTime"), rs.getString("attActions")); 
              
                  
                 //  System.out.println(rs.getString("Name"));
                 al=new String[]{rs.getString("Name"),rs.getString("Type"),rs.getString("Total"),rs.getString("ingedriant")};
                 als.add(al);
                 //  System.out.println(als.size());
                // System.out.println(als);
                 //count++;    
                              
                 
                      //  als.toArray();
                         
                  
               }
             //  System.out.println(als.size());
               String result[][]=new String[als.size()][];
               for (int i = 0; i < als.size(); i++) {
                 //  System.out.println(als.get(i));
                   result[i]=als.get(i);
                  
              }
               
               
               
              
               //String result[][]=(String[][]) al.toArray();
                
         String col[]={"Name","Type","Total","Ingredient"};   
//              System.out.println(result[0][0]);
              dtm=new DefaultTableModel(result,col);
              dtm.fireTableDataChanged();
               JTable jt= new JTable(dtm);
              jt.getColumn(col[3]).setPreferredWidth(300);
             // System.out.println(jt.getColumn(3).getWidth());
               jt.setOpaque(false);
            //   jt.setOpaque(true);
            
               
              // jt.setBackground(Color.red);
               jt.setShowGrid(false);
               jt.setForeground(Color.WHITE);
            //   jt.setFont(BakeryController.FredokaOne());
              jt.setSelectionBackground(new Color(211,202,178));
              jt.setSelectionForeground(Color.black);
              jt.setBackground(new Color(0,0,0,0));
               JScrollPane jp= new JScrollPane(jt);
              
               jp.setHorizontalScrollBarPolicy(jp.HORIZONTAL_SCROLLBAR_ALWAYS);
               
              // jt.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
              jp.getViewport().setBackground(new
Color(0,0,0,0));
               jp.setBounds(0, 0, 350, 390);
               jp.setOpaque(!isOpaque());
               jp.getViewport().setOpaque(!isOpaque());
               add(jp);
               jt.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               setOpaque(!isOpaque());
                  
        setVisible(true);
    }
 public static void tablegenerator(int quantity ,String name){
        try {
           
                java.sql.Connection con= BakeryController.databaseconnection();
               
            String query= "update ingedrianttable set total=total - ? where ingedriantname = ? AND total>0;";
          
              PreparedStatement pstmup= con.prepareStatement(query);
              pstmup.setInt(1, quantity);
             
            String[] al= null;
            
            //             System.out.println("RS"+ rs.last());
            ResultSet rs= BakeryController.inventoryselector(name);
            int count=0;
            //int i=0;
            ArrayList<String[]> als= new ArrayList<>();
            while(rs.next())
            {
                //   al=new ArrayList(rs.getString("AtdDate"), rs.getString("attTime"), rs.getString("attActions"));
                
                
                //  System.out.println(rs.getString("Name"));
                al=new String[]{rs.getString("Name"),rs.getString("Type"),rs.getString("Total"),rs.getString("ingedriant")};
                als.add(al);
                //  System.out.println(als.size());
                // System.out.println(als);
                //count++;
                
   
                //  als.toArray();
                
                
            }
            //  System.out.println(als.size());
            String result[][]=new String[als.size()][];
            for (int i = 0; i < als.size(); i++) {
                //  System.out.println(als.get(i));
                result[i]=als.get(i);
                
            }
            
            
            
            
            //String result[][]=(String[][]) al.toArray();
            
            String col[]={"Name","Type","Total","Ingredient"};
//              System.out.println(result[0][0]);
dtm=new DefaultTableModel(result,col);
ArrayList<String>as= new ArrayList<String>();
String []aa;

for(int i=0;i<dtm.getRowCount();i++) {
    
    String s=(String)dtm.getValueAt(i, 3);
    as.add(s);
    
    
    
    
    
}
aa=as.toArray(new String[as.size()]);
String[] s=null;
//aa=s.split(",");
for(int a=0; a< aa.length;a++)
{
    
    s=aa[a].toString().split(",");
    
}
for (int c=0;c<s.length;c++)
{
   
    pstmup.setString(2, s[c].toString());
    pstmup.executeUpdate();
   
}
        } catch (SQLException ex) {
           ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
        } catch (ClassNotFoundException ex) {
            
        }
 }
 public static void refresh(){
     dtm.fireTableDataChanged();
     System.err.println("Refreshed");
 }
 
}
