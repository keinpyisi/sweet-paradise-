
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krul
 */
public class receiptpane extends JPanel{

    /**
     * @param args the command line arguments
     */
  
	static String   header[]= {"Name","Quantity","Prize"};
	 private static DefaultTableModel dtm = new DefaultTableModel(null, header);
         private static JTable table = new JTable(dtm);
   
public static int q,w,e;
          private static final int N_ROWS = 8;
         
    private JScrollPane scrollPane = new JScrollPane(table);
    private JScrollBar vScroll = scrollPane.getVerticalScrollBar();
    
    private int row;
    private boolean isAutoScroll;
    public static void main(String[] args) {
        
            try {
                new receiptpane();
            } catch (SQLException ex) {
               ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                  
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
            } catch (ClassNotFoundException ex) {
             
            }
       
    }
    
    public receiptpane() throws SQLException, ClassNotFoundException{
        setSize(200, 550);
    
        setLayout(new BorderLayout());
        
        
        table.getTableHeader().getColumnModel().getColumn(0).setWidth(120);
        table.getTableHeader().getColumnModel().getColumn(1).setWidth(80);
        
        table.getTableHeader().getColumnModel().getColumn(2).setWidth(80);
       getdata();
               
        
           
	//	String data[][]= {};
                 Dimension d = new Dimension(320, N_ROWS * table.getRowHeight());
        table.setPreferredScrollableViewportSize(d);
      
        scrollPane.setVerticalScrollBarPolicy(
            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        vScroll.addAdjustmentListener(new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                isAutoScroll = !e.getValueIsAdjusting();
            }
        });
        //scrollPane.setBounds(0, 0, 303, 350);
        scrollPane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        this.add(scrollPane);
		dtm.fireTableDataChanged();

 revalidate();
                repaint();
                setVisible(true);
					}
    
       
      
   
                
        
        
     public static void addRow(String name, String Quantity, String Amount) {
        
        dtm.addRow(new Object[]{
                String.valueOf(name),
                String.valueOf(Quantity) , String.valueOf(Amount)
                
            });
       
        dtm.fireTableDataChanged();
        
    }
     public static void countcolumn(String quantity,String total,String condition){
       try{  for(int i=0;i<36;i++) {
				for(int j=0;j<36;j++) {
                                  
					if(dtm.getValueAt(j, 0).equals(condition)==true) {
					                  
					
						dtm.setValueAt(quantity, j, 1);
                                                dtm.setValueAt(total, j, 2);
						dtm.fireTableDataChanged();
                                               
					}
				}
         }
       }catch(ArrayIndexOutOfBoundsException a)
       {
           
       }
     }
     public static int Total() {
		q=0;
		for(int i=0;i<dtm.getRowCount();i++) {
			
				q=q+Integer.parseInt((String)dtm.getValueAt(i, 2)) ;
				
				
			}
		return q;
		
		}
    
     public static void clear(){
         dtm.getDataVector().removeAllElements();
         cleardata();
				dtm.fireTableDataChanged();
				table.repaint();
                                
                                
     }
     public static final String cleartable="DELETE FROM receipt WHERE 1=1";
     public static void cleardata(){
            try {
                Connection con= BakeryController.createConnection();
                PreparedStatement p=con.prepareCall(cleartable);
                p.executeUpdate();
                con.close();
               
            } catch (SQLException ex) {
                
            }
     }
     public static final String updatePerson="update receipt set quality = ? , price = ? where name= ? ";
     public static void updateData(String name, String quality , String price)
     {
            try {
                Connection con=BakeryController.createConnection();
                //System.out.println("data inserted");
                PreparedStatement p=con.prepareStatement(updatePerson);
                p.setString(1, quality);
                p.setString(2, price);
                p.setString(3, name);
              
                p.executeUpdate();
                con.close();
            } catch (SQLException ex) {
               ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
            }
     }
      public static final String insertPerson="INSERT INTO receipt (name,quality,price) VALUES (?,?,?)";
    public static void insertData(String name , String quality, String price) {
		try {
                    
			Connection con=BakeryController.createConnection();
			//System.out.println("data inserted");
                        PreparedStatement p=con.prepareStatement(insertPerson);
                         p.setString(1, name);
                     p.setString(2, quality);
                     p.setString(3, price);
		
         
         
         
         
         
                   
              
             
             
                  
                      
		p.executeUpdate();
	//	System.out.println("data inserted");
	con.commit();
        
        p.close();
		con.close();
                
		}catch(Exception e) {
		}
                
		}
     public static final String searchPerson="select * from receipt";
    public static void getdata(){
           
        
    }
    

}
        
    
    


