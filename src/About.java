/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import java.awt.FontFormatException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import static java.lang.Boolean.FALSE;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JPanel;

/**
 *
 * @author MCMITS User -2
 */
public class About extends JPanel{
    JTextArea jtf;
    JLabel jl1,jl2;
    JScrollPane js;
    JButton back;
    public static void main(String args[]){
        new About();
        
    }
    public About(){
        try {
            //  setTitle("About");
            setSize(900,600);
            //    setDefaultCloseOperation(EXIT_ON_CLOSE);
            //  setLocationRelativeTo(null);
            setLayout(null);
           // setBackground(Color.BLACK);
            
            jl1=new JLabel("About Our Shop");
            jl1.setBounds(50,1,200,40);
           // jl1.setForeground(Color.WHITE);
            jl1.setFont(BakeryController.Geometry());
            add(jl1);
            
            jtf=new JTextArea("Café Sweets Bakery caters to customers with diverse backgrounds and lifestyles. It is our goal to make.\n"+"the process of placing an order easy and convenient for you. In order to provide you with exceptional.\n"+" service and deliver the best freshly baked products, please review our Customer Service policy listed \n"+" below.\n" +
                    "\n" +
                    "Pick-Up Orders\n\n" +
                    "All orders made online are available for pick up at our bakery location 505 Clematis Street, West Palm \n"+" Beach, Florida within 1 hour of placing the order during normal store hours MTW 7:30AM-6:00PM,\n"+" THFS 7:30AM-10:30PM. Since we bake fresh daily, any orders consisting of 3 dozen or more, are treated\n"+" as special orders and are subject to availability for same day pick up, but are always available within a\n"+" 24 hour notice.\n" +
                    "\n" +
                    "Advance Orders\n\n" +
                    "We are delighted to fulfill any advance orders. We take advance orders up to 120 days in advance when \n"+"you need to plan for your perfect event. We do require that payment be made in full for all advance orders\n"+" 45 days prior to date of the event.\n" +
                    "\n" +
                    "\n" +
                    "Special Orders (24-hour advance notice required)\n\n" +
                    "Special orders are a dream for Pastry Chef Sharlyn Davis! Special orders can take on many different forms and this is an opportunity for Sharlyn to put her love for\n"+" creativity and pastry design skills to work.  Any pastry order of 3 dozen or more is considered a special\n"+" order, as we bake a small quantity of products fresh daily. We want to be sure your special order of\n"+" choice is available and we require 24-hour advance notice. Other examples of special orders would be\n"+" dietary restrictions, custom flavors, or custom designs.\n" +
                    "\n" +
                    "Delivery Policy\n\n" +
                    "We are currently evaluating demand for delivery. At this time, we are happy to deliver any special order \n"+"within 24 hours of when the order is placed and delivery is within a 3-5 mile radius of our bakery\n"+" location. There is no delivery charge under these conditions.");
            jtf.setBounds(10,30,860,500);
           // jtf.setBackground(Color.BLACK);
            jtf.setEditable(FALSE);
            jtf.setFont(BakeryController.Geometry());
            jtf.setForeground(Color.BLACK);
            add(jtf);
            
            js = new JScrollPane (jtf);
            js.setBounds(10,30,860,500);
            add(js);
            back=new JButton("Back");
            back.setBounds(10, 530, 100, 20);
            back.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                      try {
              removeAll();
              repaint();
              revalidate();
             
              JPanel a= new MainPanel();
             
              a.setBounds(0, 0, 900  , 600);
             
              add(a);
              
              
              
              
              repaint();
              validate();
          } catch (IOException ex) {
            
          } catch (FontFormatException ex) {
            
          }
             
                
                }
            });
            add(back);
            
            //   setVisible(true);
        } catch (Exception ex) {
          
        } 
    }
    
}
