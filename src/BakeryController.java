/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.mysql.jdbc.Connection;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author krul
 */
public class BakeryController {

    /**
     * @param args the command line arguments
     */
    public static final String personCreate="create table receipt(name varchar(100) ,quality varchar(100),price varchar(100))";
private static String ip;
    public static java.sql.Connection createConnection() {
		try {
			java.sql.Connection con=DriverManager.getConnection("jdbc:derby:bakeryreceipt;create=true;");
			//System.out.println("Connection success....");
			return con;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
    public static void createTable() {
		try {
			java.sql.Connection con=createConnection();
			Statement st=con.createStatement();
			st.executeUpdate(personCreate);
			
			st.close();
			con.close();
                        
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
    public static void drop(){
        try {
            java.sql.Connection con=createConnection();
            Statement st=con.createStatement();
           // st.executeUpdate("Delete From receipt where 1=1");
           st.executeUpdate("Drop table receipt");
        
            st.close();
            con.close();
        } catch (SQLException ex) {
      
        }
    }
    public static void updatederby(String quality , String price, String condition){
        try {
            String bait="update receipt set quality = ? , price = ? where name = "+condition+"";
            java.sql.Connection con=createConnection();
            PreparedStatement st=con.prepareStatement(bait);
            st.setString(1, quality);
            st.setString(2, price);
            st.executeUpdate();
       
            
        } catch (SQLException ex) {
         //  
        }
        
    }
    public static int selectderby( String condition){
      int i=0;
        try {
           
            String bait="select count(*) from receipt where name = \'"+condition+"\'";
        
            java.sql.Connection con=createConnection();
            PreparedStatement st=con.prepareStatement(bait);
             ResultSet rs= st.executeQuery();
             while(rs.next()){
             //   System.out.println(rs.getString(1)+"A");
               i = rs.getInt(1);  
             }
          
              
           
      
            
        } catch (SQLException ex) {
         //  
         ex.printStackTrace();
        }
         return i;
    }
      public static int selectcountderby( String condition){
      int i=0;
        try {
           
            String bait="select quality from receipt where name = \'"+condition+"\'";
        
            java.sql.Connection con=createConnection();
            PreparedStatement st=con.prepareStatement(bait);
             ResultSet rs= st.executeQuery();
             while(rs.next()){
             //   System.out.println(rs.getString(1)+"A");
               i = rs.getInt(1);  
             }
          
              
           
      
            
        } catch (SQLException ex) {
         //  
         ex.printStackTrace();
        }
         return i;
    }
    public static void makeIP(){
        try {
            InputStream resource =new FileInputStream(new File("C:\\Windows\\Temp\\IP.txt"));
           BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
            //StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
        ip=line;
            System.out.println("IP = " + ip) ;
        break;
           // out.append(line);
        }
       
        //    System.out.println("MakeIP" +ip);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BakeryController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BakeryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    public static Connection databaseconnection() throws ClassNotFoundException, SQLException{
       
        //    File file = new File(FileMaker.class.getResource("IP.txt").toURI());
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://"+ip+":3306/bakery?"+"user=krul&password=hatsunemiku1");
            // System.out.println("Established");
            return (Connection)con;
      
        
        
    }
    public static ResultSet receiptstatement() throws ClassNotFoundException, SQLException{
      
          //  databaseconnection();
          String bait="select * from sale;";
              java.sql.Connection con= BakeryController.databaseconnection();
             PreparedStatement smt = con.prepareStatement(bait);
            ResultSet r= smt.executeQuery(bait);
            return r;
           
    }
    public static void receiptdeletestatement() throws ClassNotFoundException, SQLException{
      
          //  databaseconnection();
          String bait="select * from sale;";
              java.sql.Connection con= BakeryController.databaseconnection();
            PreparedStatement st = con.prepareStatement("DELETE FROM sale WHERE name = name");

st.executeUpdate(); 

           
    }
    
    public static ResultSet inventorystatement() throws SQLException, ClassNotFoundException{
          String bait="SELECT * FROM inventory where name=name order by name asc;"  ;

              java.sql.Connection con= BakeryController.databaseconnection();
               
              PreparedStatement stmt=  con.prepareStatement(bait);
              //System.out.println("AAA");
               
               ResultSet rs= stmt.executeQuery(bait);
               return rs;
    }
     public static ResultSet ingredientstatement() throws SQLException, ClassNotFoundException{
          String bait="SELECT * FROM ingedrianttable where ingedriantname=ingedriantname order by ingedriantname asc;"  ;

              java.sql.Connection con= BakeryController.databaseconnection();
               
              PreparedStatement stmt=  con.prepareStatement(bait);
              //System.out.println("AAA");
               
               ResultSet rs= stmt.executeQuery(bait);
               return rs;
    }
      public static ResultSet itemstatement()  throws ClassNotFoundException, SQLException{
        
            String bait="SELECT name FROM inventory where name=name order by name asc;"  ;
            
            java.sql.Connection con= BakeryController.databaseconnection();
            
            PreparedStatement stmt=  con.prepareStatement(bait);
            //System.out.println("AAA");
            
            ResultSet rs= stmt.executeQuery(bait);
            return rs;
       
    }
    public static ResultSet admin(String user) throws ClassNotFoundException, SQLException{
         String bait="select * from login where username=?"  ;

              java.sql.Connection con= BakeryController.databaseconnection();
               
              PreparedStatement stmt=  con.prepareStatement(bait);
              stmt.setString(1, user);
              //System.out.println("AAA");
               
               ResultSet rs= stmt.executeQuery();
               return rs;
        
    }
    public static ResultSet namegetter(String id) throws SQLException, ClassNotFoundException {
        String bait= "select distinct main.name ,main.price from main where main.name= ? ; ";
         java.sql.Connection con= BakeryController.databaseconnection();
               
              PreparedStatement stmt=  con.prepareStatement(bait);
              stmt.setString(1, id);
              //stmt.setString(2, id);
              //System.out.println("AAA");
               
               ResultSet rs= stmt.executeQuery();
               return rs;
        
    }
    public static int quantitygetter(String id) 
    {
        try {
            String bait="select total from inventory inner join main on main.name= ? and inventory.name= ? ;";
            java.sql.Connection con= BakeryController.databaseconnection();
            PreparedStatement stmt= con.prepareStatement(bait);
            stmt.setString(1, id);
            stmt.setString(2, id);
            ResultSet rs= stmt.executeQuery();
            int quantity = 0;
            while (rs.next())
            {
                quantity=rs.getInt(1);
            }
            return quantity;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
           
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
        return 0;
    }
    public static ResultSet getTotal(String name) throws ClassNotFoundException, SQLException
    {
        String bait="select total from inventory where name=?"  ;

              java.sql.Connection con= BakeryController.databaseconnection();
               
              PreparedStatement stmt=  con.prepareStatement(bait);
              stmt.setString(1, name);
              //System.out.println("AAA");
               
               ResultSet rs= stmt.executeQuery();
               return rs;   
    }
    public static ResultSet attributedata(String attribute) throws SQLException, ClassNotFoundException
    {
       String bait="select * from login where username=?"  ;

              java.sql.Connection con= BakeryController.databaseconnection();
               
              PreparedStatement stmt=  con.prepareStatement(bait);
              stmt.setString(1, attribute);
              //System.out.println("AAA");
               
               ResultSet rs= stmt.executeQuery();
               return rs;   
    }
    public static ResultSet ingredeiantname() throws SQLException, ClassNotFoundException
    {
        String bait=" SELECT ingedriantname from ingedrianttable where ingedriantname=ingedriantname order by ingedriantname asc;";
         java.sql.Connection con= BakeryController.databaseconnection();
               
              PreparedStatement stmt=  con.prepareStatement(bait);
            //  stmt.setString(1, attribute);
              //System.out.println("AAA");
               
               ResultSet rs= stmt.executeQuery();
               return rs;   
        
    }
     public static ResultSet typename() throws SQLException, ClassNotFoundException
    {
        String bait="SELECT distinct type FROM bakery.main where type = type order by type asc;";
         java.sql.Connection con= BakeryController.databaseconnection();
               
              PreparedStatement stmt=  con.prepareStatement(bait);
            //  stmt.setString(1, attribute);
              //System.out.println("AAA");
               
               ResultSet rs= stmt.executeQuery();
               return rs;   
        
    }
    public static void updateingredient(String name , int total) throws SQLException, ClassNotFoundException
    {
        String bait= "UPDATE ingedrianttable SET total= total + ? WHERE ingedriantname = ? ;";
        java.sql.Connection con= BakeryController.databaseconnection();
         PreparedStatement pstm= con.prepareStatement(bait);
         pstm.setInt(1, total);
         pstm.setString(2, name);
         pstm.executeUpdate();
        
    }
      public static void addmenuitem(String name , String type , String price , String attribute ,FileInputStream imagedata) throws SQLException, ClassNotFoundException
    {
        String bait= "INSERT INTO bakery.main (type, name, price, attribute, Image) VALUES (?, ?, ?, ?, ?);";
        java.sql.Connection con= BakeryController.databaseconnection();
         PreparedStatement pstm= con.prepareStatement(bait);
         pstm.setString(1, type);
         pstm.setString(2, name);
         pstm.setString(3, price);
         pstm.setString(4, attribute);
         pstm.setBlob(5, imagedata);
         pstm.executeUpdate();
        
    }
     public static void updateitem(String name , int total) throws SQLException, ClassNotFoundException
    {
        String bait= "UPDATE  inventory SET total= total + ? WHERE name = ? ;";
        java.sql.Connection con= BakeryController.databaseconnection();
         PreparedStatement pstm= con.prepareStatement(bait);
         pstm.setInt(1, total);
         pstm.setString(2, name);
         pstm.executeUpdate();
        
    }
    public static ArrayList inventoryzerochecker() throws ClassNotFoundException{
        try {
            java.sql.Connection con= BakeryController.databaseconnection();
            String b= " SELECT ingedriantname from ingedrianttable where total=0;";
            
            
            PreparedStatement pstm= con.prepareStatement(b);
            ResultSet ss= pstm.executeQuery();
            ArrayList<String> zero= new ArrayList<>();
            while(ss.next())
            {
                zero.add(ss.getString("ingedriantname"));
            }
           
      if(!zero.isEmpty())
      {
      
          return zero;
      }
        } catch (SQLException ex) {
           
        }
        ArrayList<String>n=new ArrayList<>();
            n.add("null");
      //  System.out.println("Empty?");
return n;
    }
    public static ArrayList gettingzeroname () throws ClassNotFoundException{
        
              ArrayList<String> inventory = inventoryzerochecker();
             
              if(inventory.contains(null))
              {
                 
              }else{
               try {
            String query= "select name from inventory where ingedriant like ?;";
            java.sql.Connection con= BakeryController.databaseconnection();
          
            
            PreparedStatement pstm= con.prepareStatement(query);
            ArrayList <String> name= new ArrayList<>();
            ArrayList<String> nam= new ArrayList<>();
            for(int i=0;i<inventory.size();i++)
            {
               
                nam.add(inventory.get(i).toString());
                
                
            }
            
           
            pstm.setString(1, "%"+nam.get(0).toString()+"%");
            
          ResultSet  ss= pstm.executeQuery();           
           while(ss.next())
           {
            
                name.add(ss.getString(1));   
           }
            
               
            
            
            
            
            
            return name;
        } catch (SQLException ex) {
           
        }
}
                    return null;
    }
    public static Date convertToDateViaSqlDate(LocalDate dateToConvert) {
    return java.sql.Date.valueOf(dateToConvert);
}

    public static ResultSet inventoryselector(String name)throws SQLException, ClassNotFoundException{
        

              java.sql.Connection con= BakeryController.databaseconnection();
               
             
             
              String bait="select * from inventory where name= '" + name + "';";
               PreparedStatement stmt=  con.prepareStatement(bait);
              // stmt.setString(1, name);
               ResultSet rs= stmt.executeQuery(bait);
               return rs;
        
    }
      public static ResultSet IMAGESELECTOR(String type){
        
ResultSet rs = null;
        try {
            java.sql.Connection con= BakeryController.databaseconnection();
            
            
            
            String bait="select name , Image from main where type= '" + type + "';";
            PreparedStatement stmt=  con.prepareStatement(bait);
            // stmt.setString(1, name);
             rs= stmt.executeQuery(bait);
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BakeryController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(BakeryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
       public static int countmain(String type){
        int a=0;
ResultSet rs = null;
        try {
            java.sql.Connection con= BakeryController.databaseconnection();
            
            
            
            String bait="select count(name) from main where type= '" + type + "';";
            PreparedStatement stmt=  con.prepareStatement(bait);
            // stmt.setString(1, name);
             rs= stmt.executeQuery(bait);
             while(rs.next()){
                 a= rs.getInt(1);
             }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BakeryController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(BakeryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a;
    }
        public static int QUANITITYIMAGESELECTOR(String type){
        
 int count = 0;
        try {
            java.sql.Connection con= BakeryController.databaseconnection();
            
            
           
            String bait="SELECT count(name) FROM bakery.main where type = '" + type + "';";
            PreparedStatement stmt=  con.prepareStatement(bait);
            // stmt.setString(1, name);
            ResultSet rs= stmt.executeQuery(bait);
            while(rs.next()){
                count = rs.getInt(1);
            }
           
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BakeryController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(BakeryController.class.getName()).log(Level.SEVERE, null, ex);
        }
         return count;
    }
      

    public static void derbytosql(String UID) throws  ClassNotFoundException{
        try {
            
            String bait="select * from receipt";
            String setter="set sql_safe_updates = 0;";
           
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate now = LocalDate.now();
           
            Date date= convertToDateViaSqlDate(now);
            java.sql.Connection cons =  BakeryController.createConnection();
            java.sql.Connection con= BakeryController.databaseconnection();
           
             PreparedStatement p= con.prepareStatement(setter);
            p.executeQuery(); 
            
            Statement stmt= cons.createStatement();
            //   stmt.execute(bait);
            ResultSet rs= stmt.executeQuery(bait);
          while(rs.next()){
           //INSERT INTO `bakery`.`sale` (`price`, `quantity`, `UID`, `saledate`) VALUES ('S', 'AS', 'A', 'A');
PreparedStatement updateemp = con.prepareStatement(
         "INSERT INTO bakery.sale (price, quantity, UID, saledate,name) VALUES (?, ?, ?, ?,?)");
String name = rs.getString("name");
String price = rs.getString("price");
String quality = rs.getString("quality");
         updateemp.setString(1, price);
updateemp.setString(2, quality);
updateemp.setString(3, UID);
updateemp.setDate(4, date);
updateemp.setString(5, name);
updateemp.executeUpdate();
     
          }
            
            
          
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
    }
    // ingredientpanel.tablegenerator(Integer.parseInt(quantity), name);
             
    public static byte[] getQRCodeImage(String text, int width, int height) throws WriterException, IOException {
    QRCodeWriter qrCodeWriter = new QRCodeWriter();
    BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
    
    ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
    MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
    byte[] pngData = pngOutputStream.toByteArray(); 
    return pngData;
}
    public static Font acherus() throws FontFormatException, IOException{
        String fontFileName = "acherus.otf";
        
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.BOLD, 24);
    return ttfReal;
    } 
    public static Font Bellota() throws FontFormatException, IOException{
        String fontFileName = "Bellota.otf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    } 
    public static Font BlackCasper() throws FontFormatException, IOException{
        String fontFileName = "BlackCasper.ttf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    } 
    public static Font bubblerone() throws FontFormatException, IOException{
        String fontFileName = "bubblerone.ttf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    } 
    public static Font DistrictPro() throws FontFormatException, IOException{
        String fontFileName = "DistrictPro.otf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    } 
     public static Font FredokaOne() throws FontFormatException, IOException{
        String fontFileName = "FredokaOne-Regular.otf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    } 
     public static Font Geometry() throws FontFormatException, IOException{
        String fontFileName = "Geometry.otf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    } 
     public static Font Gistlight() throws FontFormatException, IOException{
        String fontFileName = "GistLight.otf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    } 
     public static Font HappyMonkey() throws FontFormatException, IOException{
        String fontFileName = "HappyMonkey.ttf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 14);
    return ttfReal;
    }
     public static Font littletrouble() throws FontFormatException, IOException{
        String fontFileName = "littletroublegirl.ttf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    }
     public static Font Zawgyi() throws FontFormatException, IOException{
        String fontFileName = "Zawgyi-One.ttf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 24);
    return ttfReal;
    }
     
      public static Font Emoji() throws FontFormatException, IOException{
        String fontFileName = "emoji.otf";
    InputStream is = BakeryController.class.getResourceAsStream("font/"+fontFileName);
        
    Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, is);
    

    Font ttfReal = ttfBase.deriveFont(Font.PLAIN, 34);
    return ttfReal;
    }
   public static Color red(){
        
 Color c= Color.decode("#FF0000");
         
return c;
     
}
public static Color white(){
        
 Color c= Color.decode("#FFFFFF");
         
return c;
     
}
public static Color Cyan(){
        
 Color c= Color.decode("#00FFFF");
         
return c;
     
}
public static Color Silver(){
        
 Color c= Color.decode("#C0C0C0");
         
return c;
     
}
public static Color Blue(){
        
 Color c= Color.decode("#0000FF");
         
return c;
     
}
public static Color Light_Blue(){
        
 Color c= Color.decode("#ADD8E6");
         
return c;
     
}
public static Color Orange(){
        
 Color c= Color.decode("#FFA500");
         
return c;
     
}
public static Color Purple(){
        
 Color c= Color.decode("#800080");
         
return c;
     
}
public static Color brown(){
        
 Color c= Color.decode("#A52A2A");
         
return c;
     
}
public static Color yellow(){
        
 Color c= Color.decode("#FFFF00");
         
return c;
     
}
public static Color maroon(){
        
 Color c= Color.decode("#800000");
         
return c;
     
}
public static Color lime(){
        
 Color c= Color.decode("#00FF00");
         
return c;
     
}
public static Color green(){
        
 Color c= Color.decode("#008000");
         
return c;
     
}
public static Color mangenta(){
        
 Color c= Color.decode("#FF00FF");
         
return c;
     
}
public static Color Oliva(){
        
 Color c= Color.decode("#808000");
         
return c;
     
}
public static Color black(){
        
 Color c= Color.decode("#000000");
         
return c;
     
}
public static Color night(){
        
 Color c= Color.decode("#0C090A");
         
return c;
     
}
public static Color gunmetal(){
        
 Color c= Color.decode("#2C3539");
         
return c;
     
}
public static Color midnight(){
        
 Color c= Color.decode("#2B1B17");
         
return c;
     
}
public static Color charcoal(){
        
 Color c= Color.decode("#34282C");
         
return c;
     
}
public static Color dark_slate_grey(){
        
 Color c= Color.decode("#25383C");
         
return c;
     
}
public static Color oil(){
        
 Color c= Color.decode("#3B3131");
         
return c;
     
}
public static Color black_cat(){
        
 Color c= Color.decode("#413839");
         
return c;
     
}
public static Color iridium(){
        
 Color c= Color.decode("#3D3C3A ");
         
return c;
     
}
public static Color black_Eel(){
        
 Color c= Color.decode("#463E3F");
         
return c;
     
}
public static Color black_cow(){
        
 Color c= Color.decode("#4C4646");
         
return c;
     
}
public static Color gray_wolf(){
        
 Color c= Color.decode("#504A4B");
         
return c;
     
}
public static Color vampire_gray(){
        
 Color c= Color.decode("#565051");
         
return c;
     
}
public static Color graydolphin(){
        
 Color c= Color.decode("#5C5858");
         
return c;
     
}
public static Color ash_gray(){
        
 Color c= Color.decode("##666362");
         
return c;
     
}
public static Color cloudy_gray(){
        
 Color c= Color.decode("#6D6968");
         
return c;
     
}
public static Color smokey_gray(){
        
 Color c= Color.decode("#726E6D");
         
return c;
     
}
public static Color gray(){
        
 Color c= Color.decode("#736F6E");
         
return c;
     
}
public static Color granite(){
        
 Color c= Color.decode("#837E7C");
         
return c;
     
}
public static Color Battleship_gray(){
        
 Color c= Color.decode("#848482");
         
return c;
     
}
public static Color gray_cloud(){
        
 Color c= Color.decode("#B6B6B4");
         
return c;
     
}
public static Color gray_goose(){
        
 Color c= Color.decode("#D1D0CE");
         
return c;
     
}
public static Color Platinum(){
        
 Color c= Color.decode("#E5E4E2");
         
return c;
     
}
public static Color Metallic_silver(){
        
 Color c= Color.decode("#BCC6CC");
         
return c;
     
}
public static Color Blue_gray(){
        
 Color c= Color.decode("#98AFC7");
         
return c;
     
}
public static Color Light_slate_gray(){
        
 Color c= Color.decode("#6D7B8D");
         
return c;
     
}
public static Color Slate_gray(){
        
 Color c= Color.decode("#657383");
         
return c;
     
}
public static Color Jet_gray(){
        
 Color c= Color.decode("#616D7E");
         
return c;
     
}
public static Color Mist_blue(){
        
 Color c= Color.decode("#646D7E");
         
return c;
     
}
public static Color Marble_blue(){
        
 Color c= Color.decode("#566D7E");
         
return c;
     
}
public static Color Slate_blue(){
        
 Color c= Color.decode("#737CA1");
         
return c;
     
}
public static Color Steel_blue(){
        
 Color c= Color.decode("#4863A0");
         
return c;
     
}
public static Color Blue_jay(){
        
 Color c= Color.decode("#2B547E");
         
return c;
     
}
public static Color dark_slate_blue(){
        
 Color c= Color.decode("#2B3856");
         
return c;
     
}
public static Color midnight_blue(){
        
 Color c= Color.decode("#151B54");
         
return c;
     
}
public static Color Navy_blue(){
        
 Color c= Color.decode("#000080");
         
return c;
     
}
public static Color Blue_whale(){
        
 Color c= Color.decode("#342D7E");
         
return c;
     
}
public static Color Lapis_blue(){
        
 Color c= Color.decode("#15317E");
         
return c;
     
}
public static Color Denim_dark_blue(){
        
 Color c= Color.decode("#151B8D");
         
return c;
     
}
public static Color Earth_blue(){
        
 Color c= Color.decode("#0000A0");
         
return c;
     
}
public static Color Cobalt_blue(){
        
 Color c= Color.decode("#0020C2");
         
return c;
     
}
public static Color Blueberry_blue(){
        
 Color c= Color.decode("#0041C2");
         
return c;
     
}
public static Color Sapphire_blue(){
        
 Color c= Color.decode("#2554C7");
         
return c;
     
}
public static Color blue_Eyes(){
        
 Color c= Color.decode("#1569C7");
         
return c;
     
}
public static Color Royal_blue(){
        
 Color c= Color.decode("#2B60DE");
         
return c;
     
}
public static Color Blue_orchid(){
        
 Color c= Color.decode("#1F45FC");
         
return c;
     
}
public static Color Blue_lotus(){
        
 Color c= Color.decode("#6960EC");
         
return c;
     
}
public static Color Light_slate_blue(){
        
 Color c= Color.decode("#736AFF");
         
return c;
     
}
public static Color Windows_blue(){
        
 Color c= Color.decode("#357EC7");
         
return c;
     
}
public static Color Glacial_blue_Ice(){
        
 Color c= Color.decode("#368BC1 ");
         
return c;
     
}
public static Color Silk_blue(){
        
 Color c= Color.decode("#488AC7");
         
return c;
     
}
public static Color Blue_Ivy(){
        
 Color c= Color.decode("#3090C7");
         
return c;
     
}
public static Color Blue_Koi(){
        
 Color c= Color.decode("#3090C7");
         
return c;
     
}
public static Color Columbia_blue(){
        
 Color c= Color.decode("#87AFC7");
         
return c;
     
}
public static Color Baby_blue(){
        
 Color c= Color.decode("#95B9C7");
         
return c;
     
}
public static Color Light_steel_blue(){
        
 Color c= Color.decode("#728FCE");
         
return c;
     
}
public static Color Ocean_blue(){
        
 Color c= Color.decode("##2B65EC");
         
return c;
     
}
public static Color Blue_Ribbon(){
        
 Color c= Color.decode("#306EFF");
         
return c;
     
}
public static Color Blue_dress(){
        
 Color c= Color.decode("#157DEC");
         
return c;
     
}
public static Color Dodger_blue(){
        
 Color c= Color.decode("#1589FF");
         
return c;
     
}
public static Color Cornflower_blue(){
        
 Color c= Color.decode("#6495ED");
         
return c;
     
}
public static Color Sky_blue(){
        
 Color c= Color.decode("#6698FF");
         
return c;
     
}
public static Color Butterfly_blue(){
        
 Color c= Color.decode("#38ACEC");
         
return c;
     
}
public static Color Iceberg(){
        
 Color c= Color.decode("#56A5EC");
         
return c;
     
}
public static Color Crystal_blue(){
        
 Color c= Color.decode("#5CB3FF");
         
return c;
     
}
public static Color Deep_sky_blue(){
        
 Color c= Color.decode("#3BB9FF");
         
return c;
     
}
public static Color Denim_blue(){
        
 Color c= Color.decode("#79BAEC");
         
return c;
     
}
public static Color Light_sky_blue(){
        
 Color c= Color.decode("##82CAFA");
         
return c;
     
}
public static Color Day_sky_blue(){
        
 Color c= Color.decode("#82CAFF ");
         
return c;
     
}
public static Color Jeans_blue(){
        
 Color c= Color.decode("#A0CFEC");
         
return c;
     
}
public static Color Blue_Angel(){
        
 Color c= Color.decode("#B7CEEC");
         
return c;
     
}
public static Color Pastel_Blue(){
        
 Color c= Color.decode("#B4CFEC");
         
return c;
     
}
public static Color Sea_blue(){
        
 Color c= Color.decode("#C2DFFF");
         
return c;
     
}
public static Color Powder_blue(){
        
 Color c= Color.decode("#C6DEFF");
         
return c;
     
}
public static Color Coral_blue(){
        
 Color c= Color.decode("#AFDCEC");
         
return c;
     
}
public static Color Light_blue(){
        
 Color c= Color.decode("#ADDFFF");
         
return c;
     
}
public static Color Robin_Egg_Blue(){
        
 Color c= Color.decode("#BDEDFF");
         
return c;
     
}
public static Color Pale_Blue_Lily(){
        
 Color c= Color.decode("#CFECEC");
         
return c;
     
}
public static Color Light_Cyan(){
        
 Color c= Color.decode("#E0FFFF");
         
return c;
     
}
public static Color Water(){
        
 Color c= Color.decode("#EBF4FA");
         
return c;
     
}
public static Color Alice_blue(){
        
 Color c= Color.decode("#F0F8FF");
         
return c;
     
}
public static Color Azure(){
        
 Color c= Color.decode("#F0FFFF");
         
return c;
     
}
public static Color Light_slate(){
        
 Color c= Color.decode("#CCFFFF");
         
return c;
     
}
public static Color Light_Aquamarine(){
        
 Color c= Color.decode("#93FFE8");
         
return c;
     
}
public static Color Electricblue(){
        
 Color c= Color.decode("#9AFEFF");
         
return c;
     
}
public static Color Aquamarine(){
        
 Color c= Color.decode("#7FFFD4");
         
return c;
     
}
public static Color Cyan_or_Aqua(){
        
 Color c= Color.decode("#00FFFF	");
         
return c;
     
}
public static Color Tron_Blue(){
        
 Color c= Color.decode("#7DFDFE");
         
return c;
     
}
public static Color Blue_Lagoon(){
        
 Color c= Color.decode("#8EEBEC");
         
return c;
     
}
public static Color Celeste(){
        
 Color c= Color.decode("#50EBEC");
         
return c;
     
}
public static Color Blue_Diamond(){
        
 Color c= Color.decode("#4EE2EC");
         
return c;
     
}
public static Color Tiffany_blue(){
        
 Color c= Color.decode("#81D8D0");
         
return c;
     
}
public static Color Cyan_Opaque(){
        
 Color c= Color.decode("#92C7C7");
         
return c;
     
}
public static Color Blue_Hosta(){
        
 Color c= Color.decode("#77BFC7");
         
return c;
     
}
public static Color Northern_Lights_Blue(){
        
 Color c= Color.decode("#78C7C7");
         
return c;
     
}
public static Color Medium_Turq(){
        
 Color c= Color.decode("#48CCCD");
         
return c;
     
}
public static Color Turquoise(){
        
 Color c= Color.decode("#43C6DB");
         
return c;
     
}
public static Color Jelly_fish(){
        
 Color c= Color.decode("#46C7C7");
         
return c;
     
}
public static Color Blue_green(){
        
 Color c= Color.decode("#7BCCB5");
         
return c;
     
}
public static Color Sakura(){
    Color c= Color.decode("#ffb7c5");
    return c;
}
public static Color Macaw_Blue_Green(){
        
 Color c= Color.decode("#43BFC7");
         
return c;
     
}
public static Color Light_Sea_Green(){
        
 Color c= Color.decode("#3EA99F");
         
return c;
     
}
public static Color Dark_Turquoise(){
        
 Color c= Color.decode("#3B9C9C");
         
return c;
     
}
public static Color Sea_Turtle_Green(){
        
 Color c= Color.decode("#438D80");
         
return c;
     
}
public static Color Medium_Aquamarine(){
        
 Color c= Color.decode("#348781");
         
return c;
     
}
public static Color Greenish_blue(){
        
 Color c= Color.decode("#307D7E");
         
return c;
     
}
public static Color Grayish_Turquoise(){
        
 Color c= Color.decode("#5E7D7E");
         
return c;
     
}
public static Color Beetle_Green(){
        
 Color c= Color.decode("#4C787E");
         
return c;
     
}
public static Color Teal(){
        
 Color c= Color.decode("#008080");
         
return c;
     
}
public static Color Sea_Green(){
        
 Color c= Color.decode("#4E8975");
         
return c;
     
}
public static Color Camouflage_Green(){
        
 Color c= Color.decode("#78866B");
         
return c;
     
}
public static Color Sage_Green(){
        
 Color c= Color.decode("#848b79");
         
return c;
     
}
public static Color Hazel_Green(){
        
 Color c= Color.decode("#617C58");
         
return c;
     
}
public static Color Venom_Green(){
        
 Color c= Color.decode("#728C00");
         
return c;
     
}
public static Color Fern_Green(){
        
 Color c= Color.decode("#667C26");
         
return c;
     
}
public static Color Dark_Forest_Green(){
        
 Color c= Color.decode("#254117");
         
return c;
     
}
public static Color Medium_Sea_Green (){
        
 Color c= Color.decode("#306754");
         
return c;
     
}
public static Color Medium_Forest_Green(){
        
 Color c= Color.decode("#347235");
         
return c;
     
}
public static Color Seaweed_Green(){
        
 Color c= Color.decode("#437C17");
         
return c;
     
}
public static Color Pine_Green(){
        
 Color c= Color.decode("#387C44");
         
return c;
     
}
public static Color Jungle_Greeen (){
        
 Color c= Color.decode("#347C2C");
         
return c;
     
}
public static Color Shamrock_Green(){
        
 Color c= Color.decode("#347C17");
         
return c;
     
}
public static Color Medium_Spring_Green(){
        
 Color c= Color.decode("#348017");
         
return c;
     
}
public static Color Forest_Green(){
        
 Color c= Color.decode("#4E9258");
         
return c;
     
}
public static Color Green_Onion(){
        
 Color c= Color.decode("#6AA121");
         
return c;
     
}
public static Color Spring_Green(){
        
 Color c= Color.decode("#4AA02C");
         
return c;
     
}
public static Color Lime_Green(){
        
 Color c= Color.decode("#41A317");
         
return c;
     
}
public static Color Clover_Green(){
        
 Color c= Color.decode("#3EA055");
         
return c;
     
}
public static Color Green_Snake(){
        
 Color c= Color.decode("#6CBB3C");
         
return c;
     
}
public static Color Alien_Green(){
        
 Color c= Color.decode("#6CC417");
         
return c;
     
}
public static Color Green_Apple(){
        
 Color c= Color.decode("#4CC417");
         
return c;
     
}
public static Color Yellow_Green(){
        
 Color c= Color.decode("#52D017");
         
return c;
     
}
public static Color Kelly_color(){
        
 Color c= Color.decode("#4CC552");
         
return c;
     
}
public static Color Zombie_Green(){
        
 Color c= Color.decode("#54C571");
         
return c;
     
}
public static Color Frog_Green(){
        
 Color c= Color.decode("#99C68E");
         
return c;
     
}
public static Color Green_Peas(){
        
 Color c= Color.decode("#89C35C");
         
return c;
     
}
public static Color Dollar_Bill_Green(){
        
 Color c= Color.decode("#85BB65");
         
return c;
     
}
public static Color Dark_Sea_Green(){
        
 Color c= Color.decode("#8BB381");
         
return c;
     
}
public static Color Iguana_Green(){
        
 Color c= Color.decode("#9CB071");
         
return c;
     
}
public static Color Avocado_Green(){
        
 Color c= Color.decode("#B2C248");
         
return c;
     
}
public static Color Pistachio_Green(){
        
 Color c= Color.decode("#9DC209");
         
return c;
     
}
public static Color Salad_Green(){
        
 Color c= Color.decode("#A1C935");
         
return c;
     
}
public static Color  Hummingbird_GreenH(){
        
 Color c= Color.decode("#7FE817");
         
return c;
     
}
public static Color Nebula_Green(){
        
 Color c= Color.decode("#59E817");
         
return c;
     
}
public static Color Stoplight_Go_Green(){
        
 Color c= Color.decode("#57E964");
         
return c;
     
}
public static Color Algae_Green(){
        
 Color c= Color.decode("#64E986");
         
return c;
     
}
public static Color Jade_Green(){
        
 Color c= Color.decode("#5EFB6E");
         
return c;
     
}
public static Color Green(){
        
 Color c= Color.decode("#00FF00");
         
return c;
     
}
public static Color Emerald_Green(){
        
 Color c= Color.decode("#5FFB17");
         
return c;
     
}
public static Color Lawn_Green(){
        
 Color c= Color.decode("#87F717");
         
return c;
     
}
public static Color Chart_treuse(){
        
 Color c= Color.decode("#8AFB17");
         
return c;
     
}
public static Color Dragon_Green(){
        
 Color c= Color.decode("#6AFB92");
         
return c;
     
}
public static Color Mint_Green(){
        
 Color c= Color.decode("#98FF98");
         
return c;
     
}
public static Color Green_Thumb(){
        
 Color c= Color.decode("#B5EAAA");
         
return c;
     
}
public static Color Light_Jade(){
        
 Color c= Color.decode("#C3FDB8");
         
return c;
     
}
public static Color Tea_Green(){
        
 Color c= Color.decode("#CCFB5D");
         
return c;
     
}
public static Color Green_Yellow(){
        
 Color c= Color.decode("#B1FB17");
         
return c;
     
}
public static Color Slime_Green(){
        
 Color c= Color.decode("#BCE954");
         
return c;
     
}
public static Color Goldenrod(){
        
 Color c= Color.decode("#EDDA74");
         
return c;
     
}
public static Color Harvest_Gold(){
        
 Color c= Color.decode("#EDE275");
         
return c;
     
}
public static Color Sun_Yellow(){
        
 Color c= Color.decode("#FFE87C");
         
return c;
     
}
public static Color Yellow(){
        
 Color c= Color.decode("#FFFF00	");
         
return c;
     
}
public static Color Corn_Yellow(){
        
 Color c= Color.decode("#FFF380");
         
return c;
     
}
public static Color Parchment(){
        
 Color c= Color.decode("#FFFFC2");
         
return c;
     
}
public static Color Cream(){
        
 Color c= Color.decode("#FFFFCC");
         
return c;
     
}
public static Color Lemon_Chiffon(){
        
 Color c= Color.decode("#FFF8C6");
         
return c;
     
}
public static Color Cornsilk(){
        
 Color c= Color.decode("#FFF8DC");
         
return c;
     
}
public static Color Beige(){
        
 Color c= Color.decode("#F5F5DC");
         
return c;
     
}
public static Color Blonde(){
        
 Color c= Color.decode("#FBF6D9");
         
return c;
     
}
public static Color Antique_White(){
        
 Color c= Color.decode("#FAEBD7");
         
return c;
     
}
public static Color Champagne(){
        
 Color c= Color.decode("#F7E7CE ");
         
return c;
     
}
public static Color Blanched_Almond(){
        
 Color c= Color.decode("#FFEBCD");
         
return c;
     
}
public static Color Vanilla(){
        
 Color c= Color.decode("##F3E5AB");
         
return c;
     
}
public static Color Tan_Brown(){
        
 Color c= Color.decode("#ECE5B6");
         
return c;
     
}
public static Color Peach(){
        
 Color c= Color.decode("#FFE5B4");
         
return c;
     
}
public static Color Mustard(){
        
 Color c= Color.decode("#FFDB58");
         
return c;
     
}
public static Color Rubber_Ducky_Yellow(){
        
 Color c= Color.decode("#FFD801");
         
return c;
     
}
public static Color Bright_Gold(){
        
 Color c= Color.decode("#FDD017");
         
return c;
     
}
public static Color Golden_Brown(){
        
 Color c= Color.decode("#EAC117");
         
return c;
     
}
public static Color Macaroni_and_Cheese(){
        
 Color c= Color.decode("#F2BB66");
         
return c;
     
}
public static Color Saffron(){
        
 Color c= Color.decode("#FBB917");
         
return c;
     
}
public static Color Beer(){
        
 Color c= Color.decode("#FBB117");
         
return c;
     
}
public static Color Cantaloupe(){
        
 Color c= Color.decode("#FFA62F");
         
return c;
     
}
public static Color Bee_Yellow(){
        
 Color c= Color.decode("#E9AB17");
         
return c;
     
}
public static Color Brown_Sugar(){
        
 Color c= Color.decode("#E2A76F");
         
return c;
     
}
public static Color Burly_Wood(){
        
 Color c= Color.decode("#DEB887");
         
return c;
     
}
public static Color Deep_Peach(){
        
 Color c= Color.decode("#FFCBA4");
         
return c;
     
}
public static Color Ginger_Brown(){
        
 Color c= Color.decode("#C9BE62");
         
return c;
     
}
public static Color School_Bus_Yellow(){
        
 Color c= Color.decode("#E8A317");
         
return c;
     
}
public static Color Sandy_Brown(){
        
 Color c= Color.decode("#EE9A4D");
         
return c;
     
}
public static Color Fall_Leaf_Brown(){
        
 Color c= Color.decode("#C8B560");
         
return c;
     
}
public static Color Orange_Gold(){
        
 Color c= Color.decode("#D4A017 ");
         
return c;
     
}
public static Color Sand(){
        
 Color c= Color.decode("#C2B280");
         
return c;
     
}
public static Color Cookie_Brown(){
        
 Color c= Color.decode("#C7A317");
         
return c;
     
}
public static Color Caramel(){
        
 Color c= Color.decode("#C68E17");
         
return c;
     
}
public static Color Brass(){
        
 Color c= Color.decode("#B5A642");
         
return c;
     
}
public static Color Khaki(){
        
 Color c= Color.decode("#ADA96E");
         
return c;
     
}
public static Color Camel_Brown(){
        
 Color c= Color.decode("#C19A6B");
         
return c;
     
}
public static Color Bronze(){
        
 Color c= Color.decode("#CD7F32");
         
return c;
     
}
public static Color Tiger_Orange(){
        
 Color c= Color.decode("#C88141");
         
return c;
     
}
public static Color Cinnamon(){
        
 Color c= Color.decode("#C58917");
         
return c;
     
}
public static Color Bullet_Shell(){
        
 Color c= Color.decode("#AF9B60");
         
return c;
     
}
public static Color Dark_Goldenrod(){
        
 Color c= Color.decode("#AF7817");
         
return c;
     
}
public static Color Copper(){
        
 Color c= Color.decode("#B87333");
         
return c;
     
}
public static Color Wood(){
        
 Color c= Color.decode("#966F33");
         
return c;
     
}
public static Color Oak_Brown(){
        
 Color c= Color.decode("#806517");
         
return c;
     
}
public static Color Moccasin(){
        
 Color c= Color.decode("#827839");
         
return c;
     
}
public static Color Army_Brown(){
        
 Color c= Color.decode("#827B60");
         
return c;
     
}
public static Color Sandstone(){
        
 Color c= Color.decode("#786D5F");
         
return c;
     
}
public static Color Mocha(){
        
 Color c= Color.decode("#493D26");
         
return c;
     
}
public static Color Taupe(){
        
 Color c= Color.decode("#483C32");
         
return c;
     
}
public static Color Coffee(){
        
 Color c= Color.decode("#6F4E37");
         
return c;
     
}
public static Color Brown_Bear(){
        
 Color c= Color.decode("#835C3B");
         
return c;
     
}
public static Color Red_Dirt(){
        
 Color c= Color.decode("#7F5217");
         
return c;
     
}
public static Color Sepia(){
        
 Color c= Color.decode("#7F462C");
         
return c;
     
}
public static Color Orange_Salmon(){
        
 Color c= Color.decode("#C47451");
         
return c;
     
}
public static Color Rust(){
        
 Color c= Color.decode("#C36241");
         
return c;
     
}
public static Color Red_fox(){
        
 Color c= Color.decode("#C35817");
         
return c;
     
}
public static Color Chocolate(){
        
 Color c= Color.decode("#C85A17");
         
return c;
     
}
public static Color Sedona(){
        
 Color c= Color.decode("#CC6600");
         
return c;
     
}
public static Color Papaya_Orange(){
        
 Color c= Color.decode("#E56717");
         
return c;
     
}
public static Color Halloween_Orange(){
        
 Color c= Color.decode("#E66C2C");
         
return c;
     
}
public static Color Pumpkin_Orange(){
        
 Color c= Color.decode("#F87217");
         
return c;
     
}
public static Color Construction_Cone_Orange(){
        
 Color c= Color.decode("#F87431");
         
return c;
     
}
public static Color Sunrise_Orange(){
        
 Color c= Color.decode("#E67451");
         
return c;
     
}
public static Color Mango_Orange(){
        
 Color c= Color.decode("#FF8040");
         
return c;
     
}
public static Color Dark_Orange(){
        
 Color c= Color.decode("#F88017");
         
return c;
     
}
public static Color Coral(){
        
 Color c= Color.decode("#FF7F50	");
         
return c;
     
}
public static Color Basket_Ball_Orange(){
        
 Color c= Color.decode("#F87217");
         
return c;
     
}
public static Color Light_Salmon(){
        
 Color c= Color.decode("#F9966B");
         
return c;
     
}
public static Color Tangerine(){
        
 Color c= Color.decode("##E78A61");
         
return c;
     
}
public static Color Dark_Salmon(){
        
 Color c= Color.decode("#E18B6B");
         
return c;
     
}
public static Color Light_Coral(){
        
 Color c= Color.decode("#E77471");
         
return c;
     
}
public static Color Bean_Red(){
        
 Color c= Color.decode("#F75D59");
         
return c;
     
}
public static Color Valentine_Red(){
        
 Color c= Color.decode("#E55451");
         
return c;
     
}
public static Color Shocking_Orange(){
        
 Color c= Color.decode("#E55B3C");
         
return c;
     
}
public static Color Red(){
        
 Color c= Color.decode("##FF0000");
         
return c;
     
}
public static Color Scarlet(){
        
 Color c= Color.decode("#FF2400");
         
return c;
     
}
public static Color Ruby_Red(){
        
 Color c= Color.decode("#F62217");
         
return c;
     
}
public static Color Ferrari_Red(){
        
 Color c= Color.decode("#F70D1A");
         
return c;
     
}
public static Color Fire_Engine_Red(){
        
 Color c= Color.decode("#F62817");
         
return c;
     
}
public static Color Lava_Red(){
        
 Color c= Color.decode("#E42217");
         
return c;
     
}
public static Color Love_Red(){
        
 Color c= Color.decode("#E41B17");
         
return c;
     
}
public static Color Grapefruit(){
        
 Color c= Color.decode("#DC381F");
         
return c;
     
}
public static Color Chestnut_Red(){
        
 Color c= Color.decode("#C34A2C");
         
return c;
     
}
public static Color Cherry_Red(){
        
 Color c= Color.decode("#C24641");
         
return c;
     
}
public static Color Mahogany(){
        
 Color c= Color.decode("#C04000");
         
return c;
     
}
public static Color Chilli_Pepper(){
        
 Color c= Color.decode("#C11B17");
         
return c;
     
}
public static Color Cranberry(){
        
 Color c= Color.decode("#9F000F");
         
return c;
     
}
public static Color Red_Wine(){
        
 Color c= Color.decode("#990012");
         
return c;
     
}
public static Color Burgundy(){
        
 Color c= Color.decode("#8C001A");
         
return c;
     
}
public static Color Chestnut(){
        
 Color c= Color.decode("#954535 ");
         
return c;
     
}
public static Color Blood_Red(){
        
 Color c= Color.decode("#7E3517");
         
return c;
     
}
public static Color Seinna(){
        
 Color c= Color.decode("#8A4117");
         
return c;
     
}
public static Color Sangria(){
        
 Color c= Color.decode("#7E3817");
         
return c;
     
}
public static Color Firebrick(){
        
 Color c= Color.decode("#800517 ");
         
return c;
     
}
public static Color Maroon(){
        
 Color c= Color.decode("#810541");
         
return c;
     
}
public static Color Plum_Pie(){
        
 Color c= Color.decode("#7D0541");
         
return c;
     
}
public static Color Velvet_Maroon(){
        
 Color c= Color.decode("#7E354D ");
         
return c;
     
}
public static Color Plum_Velvet(){
        
 Color c= Color.decode("#7D0552");
         
return c;
     
}
public static Color Rosy_Finch(){
        
 Color c= Color.decode("#7F4E52");
         
return c;
     
}
public static Color Puce(){
        
 Color c= Color.decode("#7F5A58");
         
return c;
     
}
public static Color Dull_Purple(){
        
 Color c= Color.decode("#7F525D");
         
return c;
     
}
public static Color Rosy_Brown(){
        
 Color c= Color.decode("#B38481");
         
return c;
     
}
public static Color Khaki_Rose(){
        
 Color c= Color.decode("#C5908E");
         
return c;
     
}
public static Color Pink_Bow(){
        
 Color c= Color.decode("#C48189");
         
return c;
     
}
public static Color Lipstick_Pink(){
        
 Color c= Color.decode("#C48793");
         
return c;
     
}
public static Color Rose(){
        
 Color c= Color.decode("#E8ADAA");
         
return c;
     
}
public static Color Rose_Gold(){
        
 Color c= Color.decode("#ECC5C0");
         
return c;
     
}
public static Color Desert_Sand(){
        
 Color c= Color.decode("#EDC9AF");
         
return c;
     
}
public static Color Pig_Pink(){
        
 Color c= Color.decode("#FDD7E4");
         
return c;
     
}
public static Color Cotton_Candy(){
        
 Color c= Color.decode("#FCDFFF");
         
return c;
     
}
public static Color Pink_Bubble_Gum(){
        
 Color c= Color.decode("#FFDFDD");
         
return c;
     
}
public static Color Misty_Rose(){
        
 Color c= Color.decode("#FBBBB9");
         
return c;
     
}
public static Color Pink(){
        
 Color c= Color.decode("#FAAFBE");
         
return c;
     
}
public static Color Light_Pink(){
        
 Color c= Color.decode("#FAAFBA");
         
return c;
     
}
public static Color Flamingo_Pink(){
        
 Color c= Color.decode("#F9A7B0");
         
return c;
     
}
public static Color Pink_Rose(){
        
 Color c= Color.decode("#E7A1B0");
         
return c;
     
}
public static Color Pink_Daisy(){
        
 Color c= Color.decode("#E799A3");
         
return c;
     
}
public static Color Cadillac_Pink(){
        
 Color c= Color.decode("##E38AAE");
         
return c;
     
}
public static Color Carnation_Pink(){
        
 Color c= Color.decode("#F778A1");
         
return c;
     
}
public static Color Blush_Red(){
        
 Color c= Color.decode("#E56E94");
         
return c;
     
}
public static Color Hot_Pink(){
        
 Color c= Color.decode("#F660AB");
         
return c;
     
}
public static Color Watermelon_Pink(){
        
 Color c= Color.decode("#FC6C85");
         
return c;
     
}
public static Color Violet_Red(){
        
 Color c= Color.decode("#F6358A");
         
return c;
     
}
public static Color Deep_Pink(){
        
 Color c= Color.decode("#F52887");
         
return c;

   
}
public static Color Pink_Cupcake(){
        
 Color c= Color.decode("#E45E9D");
         
return c;
     
}
public static Color Pink_Lemonade(){
        
 Color c= Color.decode("#E4287C");
         
return c;
     
}
public static Color Neon_Pink(){
        
 Color c= Color.decode("#F535AA");
         
return c;
     
}
public static Color Magenta(){
        
 Color c= Color.decode("#FF00FF");
         
return c;
     
}
public static Color Dimorphotheca_Magenta(){
        
 Color c= Color.decode("#E3319D ");
         
return c;

   
}
public static Color Bright_Neon_Pink(){
        
 Color c= Color.decode("#F433FF	");
         
return c;
     
}
public static Color Pale_Violet_Red(){
        
 Color c= Color.decode("#D16587");
         
return c;
     
}
public static Color Tulip_Pink(){
        
 Color c= Color.decode("#C25A7C");
         
return c;
     
}
public static Color Medium_Violet_Red(){
        
 Color c= Color.decode("#CA226B");
         
return c;
     
}
public static Color Rogue_Pink(){
        
 Color c= Color.decode("#C12869");
         
return c;

   
}
public static Color Burnt_Pink(){
        
 Color c= Color.decode("#C12267");
         
return c;
     
}
public static Color Bashful_Pink(){
        
 Color c= Color.decode("#C25283");
         
return c;
     
}
public static Color Dark_Carnation_Pink(){
        
 Color c= Color.decode("#C12283");
         
return c;
     
}
public static Color Plum(){
        
 Color c= Color.decode("#B93B8F");
         
return c;
     
}
public static Color Viola_Purple(){
        
 Color c= Color.decode("#7E587E");
         
return c;

   
}
public static Color Purple_Iris(){
        
 Color c= Color.decode("#571B7E");
         
return c;
     
}
public static Color Plum_Purple(){
        
 Color c= Color.decode("#583759");
         
return c;
     
}
public static Color Indigo(){
        
 Color c= Color.decode("#4B0082");
         
return c;
     
}
public static Color Purple_Monster(){
        
 Color c= Color.decode("#461B7E");
         
return c;
     
}
public static Color Purple_Haze(){
        
 Color c= Color.decode("#4E387E");
         
return c;

   
}
public static Color Eggplant(){
        
 Color c= Color.decode("#614051");
         
return c;
     
}
public static Color Grape(){
        
 Color c= Color.decode("#5E5A80");
         
return c;
     
}
public static Color Purple_Jam(){
        
 Color c= Color.decode("#6A287E");
         
return c;
     
}
public static Color Dark_Orchid(){
        
 Color c= Color.decode("#7D1B7E");
         
return c;
     
}
public static Color Purple_Flower(){
        
 Color c= Color.decode("#A74AC7");
         
return c;

   
}
public static Color Medium_Orchid(){
        
 Color c= Color.decode("#B048B5");
         
return c;
     
}
public static Color Purple_Amethyst(){
        
 Color c= Color.decode("#6C2DC7");
         
return c;
     
}
public static Color Dark_Violet(){
        
 Color c= Color.decode("#842DCE");
         
return c;
     
}
public static Color Violet(){
        
 Color c= Color.decode("#8D38C9");
         
return c;
     
}
public static Color Purple_Sage_Bush(){
        
 Color c= Color.decode("#7A5DC7");
         
return c;

   
}
public static Color Lovely_Purple(){
        
 Color c= Color.decode("#7F38EC");
         
return c;
     
}
public static Color Purples(){
        
 Color c= Color.decode("#8E35EF");
         
return c;
     
}
public static Color Aztech_Purple(){
        
 Color c= Color.decode("#893BFF");
         
return c;
     
}
public static Color Medium_Purple(){
        
 Color c= Color.decode("#8467D7");
         
return c;
     
}
public static Color Jasmine_Purple(){
        
 Color c= Color.decode("#A23BEC");
         
return c;

   
}
public static Color Purple_Daffodil(){
        
 Color c= Color.decode("#B041FF");
         
return c;
     
}
public static Color Tyrian_Purple(){
        
 Color c= Color.decode("#C45AEC");
         
return c;
     
}
public static Color Crocus_Purple(){
        
 Color c= Color.decode("#9172EC");
         
return c;
     
}
public static Color Purple_Mimosa(){
        
 Color c= Color.decode("#9E7BFF");
         
return c;
     
}
public static Color Heliotrope_Purple(){
        
 Color c= Color.decode("#D462FF");
         
return c;

   
}
public static Color Crimson(){
        
 Color c= Color.decode("#E238EC");
         
return c;
     
}
public static Color Purple_Dragon(){
        
 Color c= Color.decode("#C38EC7");
         
return c;
     
}
public static Color Lilac(){
        
 Color c= Color.decode("#C8A2C8");
         
return c;
     
}
public static Color Blush_Pink(){
        
 Color c= Color.decode("#E6A9EC");
         
return c;
     
}
public static Color Mauve(){
        
 Color c= Color.decode("#E0B0FF");
         
return c;

   
}
public static Color Wisteria_Purple(){
        
 Color c= Color.decode("#C6AEC7 ");
         
return c;
     
}
public static Color Blossom_Pink(){
        
 Color c= Color.decode("#F9B7FF");
         
return c;
     
}
public static Color Thistle(){
        
 Color c= Color.decode("#D2B9D3");
         
return c;
     
}
public static Color Periwinkle(){
        
 Color c= Color.decode("#E9CFEC");
         
return c;
     
}
public static Color Lavender_Pinocchio(){
        
 Color c= Color.decode("#EBDDE2");
         
return c;

   
}
public static Color Lavender_blue(){
        
 Color c= Color.decode("#E3E4FA  ");
         
return c;
     
}
public static Color Pearl(){
        
 Color c= Color.decode("#FDEEF4");
         
return c;
     
}
public static Color Sea_Shell(){
        
 Color c= Color.decode("#FFF5EE");
         
return c;
     
}
public static Color Milk_White(){
        
 Color c= Color.decode("#FEFCFF");
         
return c;
     
}
public static Color White(){
        
 Color c= Color.decode("#FFFFFF");
         
return c;

   
}













}

