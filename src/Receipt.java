
import com.google.zxing.WriterException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UnsupportedLookAndFeelException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krul
 */
public class Receipt extends JPanel implements ActionListener{

    /**
     * @param args the command line arguments
     */
    private static JLabel sweet,cafe,total,mini,background,kanote;
    public static JButton bread,cake,cold,hot,back,confrim,clear,smith,snowball,spice,
            tiramisu,redvel,camat,batten,cheesecake,saltencaramal,
            callah,bangen,potatobread,beerbread,panetone,muntigranbread,bagueddeyeast,eggpie,crostata,
            coffeeamericano,hotchocolate,cappucino,espresso,irishcoffee,lattecoffee,milk,eggnog,mochachino
            ,chocolate,banana,smoothie,icecoffee,lemonicetea,strawberrymilkshake,icedcappucino,coffeesoda,greentea;
    public JPanel food,receipt,cakepanel,hotdrink,colddrink;
  private String price=null;
   static int  count=1,t=500,v=2,f=1,t1=500,v1=2,count1=1,f1=1,count3=1,t3=500,v3=2,f3=1,breadcount1=1,breadcount2=1,breadcount3=1,breadcount4=1,breadcount5=1,
           breadcount6=1,breadcount7=1,breadcount8=1,breadcount9=1,breadt1=500,breadt2=500,breadt3=500,breadt4=500,breadt5=500,breadt6=500,breadt7=500,
           breadt8=500,breadt9=500,breadf1=1,breadf2=1,breadf3=1,breadf4=1,breadf5=1,breadf6=1,breadf7=1,breadf8=1,breadf9=1,breadv1=2,breadv2=2,breadv3=2,
           breadv4=2,breadv5=2,breadv6=2,breadv7=2,breadv8=2,breadv9=2,coldcount1=1,coldcount2=1,coldcount3=1,coldcount4=1,coldcount5=1,
           coldcount6=1,coldcount7=1,coldcount8=1,coldcount9=1,coldt1=500,coldt2=500,coldt3=500,coldt4=500,coldt5=500,coldt6=500,coldt7=500,coldt8=500,
           coldt9=500,coldv1=2,coldv2=2,coldv3=2,coldv4=2,coldv5=2,coldv6=2,coldv7=2,coldv8=2,coldv9=2,coldf1=1,coldf2=1,coldf3=1,coldf4=1,coldf5=1,coldf6=1,
           coldf7=1,coldf8=1,coldf9=1,hotcount1=1,hotcount2=1,hotcount3=1,hotcount4=1,hotcount5=1,hotcount6=1,hotcount7=1,hotcount8=1,hotcount9=1,
           hott1=500,hott2=500,hott3=500,hott4=500,hott5=500,hott6=500,hott7=500,hott8=500,hott9=500,hotv1=2,hotv2=2,hotv3=2,hotv4=2,hotv5=2,hotv6=2,hotv7=2,hotv8=2,hotv9=2,
           hotf1=1,hotf2=1,hotf3=1,hotf4=1,hotf5=1,hotf6=1,hotf7=1,hotf8=1,hotf9=1,cakecount1=1,cakecount2=1,cakecount3=1,cakecount4=1,cakecount5=1,cakecount6=1,
           caket1=500,caket2=500,caket3=500,caket4=500,caket5=500,caket6=500,cakev1=2,cakev2=2,cakev3=2,cakev4=2,cakev5=2,cakev6=2,cakef1=1,cakef2=1,cakef3=1,
                   cakef4=1,cakef5=1,cakef6=1;
   static int dbcount=0,dbcount1=0,dbcount2=0,dbcount3=0,dbcount4=0,dbcount5=0,dbcount6=0,dbcount7=0,dbcount8=0,
           dbcount9=0,dbcount10=0,dbcount11=0,dbcount12=0,dbcount13=0,dbcount14=0,dbcount15=0,dbcount16=0,dbcount17=0,
           dbcount18=0,dbcount19=0,dbcount20=0,dbcount21=0,dbcount22=0,dbcount23=0,dbcount24=0,dbcount25=0,dbcount26=0,
           dbcount27=0,dbcount28=0,dbcount29=0,dbcount30=0,dbcount31=0,dbcount32=0,dbcount33=0,dbcount34=0,dbcount35=0,dbcount36=0;
    private JScrollPane foodpane,cakepane,coldpane,hotpane;
public Receipt(String condition) throws ClassNotFoundException, SQLException, FontFormatException, IOException{
    
    setSize(new Dimension(900, 600));
    setBackground(Color.BLACK);
    setLayout(null);
   // setBackground(Color.red);
   //  ImageIcon backimg= new ImageIcon(getClass().getResource(""));
      background= new JLabel("");
     background.setBounds(0, 0, 900, 600);
     background.setBackground(Color.BLACK);
     add(background);
    sweet=new JLabel("Sweet Paradise");
    sweet.setFont(BakeryController.Bellota());
    sweet.setForeground(Color.WHITE);
    sweet.setBounds(20, -10, 200, 100);
    background.add(sweet);
    
    mini=new JLabel("Desserts Make Everything Better");
    mini.setFont(new Font("Time New Roman", Font.PLAIN, 25));
    mini.setForeground(Color.WHITE);
    mini.setBounds(350, 0, 500, 100);
   background. add(mini);
    cafe=new JLabel("Bakery && Cafe");
    cafe.setBounds(50, 20, 100, 100);
    cafe.setForeground(Color.WHITE);
   background. add(cafe);
    JSeparator jsp= new JSeparator(SwingConstants.HORIZONTAL);
    jsp.setBounds(0, 100, 900, 600);
    jsp.setForeground(Color.red);
   background. add(jsp);
    bread=new JButton("BREAD");
    bread.setBounds(10, 120, 120, 50);
    bread.addActionListener(this);
   background. add(bread);
    cake=new JButton("CAKE");
    cake.setBounds(10, 180, 120, 50);
    cake.addActionListener(this);
    background.add(cake);
    cold=new JButton("COLD DRINKS");
    cold.setBounds(10, 240, 120, 50);
    cold.addActionListener(this);
   background. add(cold);
    hot=new JButton("HOT DRINKS");
    hot.setBounds(10, 300, 120, 50);
    hot.addActionListener(this);
    background.add(hot);
    JSeparator jsps= new JSeparator(SwingConstants.VERTICAL);
    jsps.setBounds(130, 107, 100, 250);
    jsps.setForeground(Color.red);
   background. add(jsps);
     JSeparator jspsp= new JSeparator(SwingConstants.HORIZONTAL);
    jspsp.setBounds(0, 360, 138, 120);
    jspsp.setForeground(Color.red);
   background. add(jspsp);
    back=new JButton("BACK");
    back.setBounds(10, 530, 100, 40);
    back.addActionListener(this);
  background.  add(back);
    JSeparator js= new JSeparator(SwingConstants.VERTICAL);
    js.setBounds(630, 107, 100, 350);
    js.setForeground(Color.red);
   background. add(js);
    ImageIcon cakeimg= new ImageIcon(getClass().getResource("img/cake.png"));
    JLabel foodpackground=new JLabel();
    foodpackground.setBackground(Color.BLACK);
    foodpackground.setBounds(0, 0, 500, 450);
   
   // System.out.println(+"COUNT");
    food=new JPanel(new GridLayout((int)Math.ceil((double)BakeryController.countmain("bread") /3),3));
    food.setBounds(135, 107, 500, 450);
   //  background.add(food);
   // food.add(foodpackground);
   int horizontalPolicy = JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED;


        int vericalPolicy = JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED;
  
   foodpane = new JScrollPane(food);
   foodpane.setBounds(135, 107, 500, 450);
   foodpane.setHorizontalScrollBarPolicy(horizontalPolicy);
   foodpane.setVerticalScrollBarPolicy(vericalPolicy);
   background.add(foodpane);
  foodpane.setVisible(false);
   cakepanel=new JPanel(new GridLayout((int)Math.ceil((double)BakeryController.countmain("cakes") /3),3));
    cakepanel.setBounds(135, 107, 500, 450);   
    
   // cakepanel.setBackground(Color.PINK);
    cakepane = new JScrollPane(cakepanel);
   cakepane.setBounds(135, 107, 500, 450);
   cakepane.setHorizontalScrollBarPolicy(horizontalPolicy);
   cakepane.setVerticalScrollBarPolicy(vericalPolicy);
   background.add(cakepane);
  cakepane.setVisible(false);
  
      
    
    hotdrink=new JPanel(new GridLayout((int)Math.ceil((double)BakeryController.countmain("hotdrink") /3),3));
    hotdrink.setBounds(135, 107, 500, 450);   
   hotpane = new JScrollPane(hotdrink);
   hotpane.setBounds(135, 107, 500, 450);
   hotpane.setHorizontalScrollBarPolicy(horizontalPolicy);
   hotpane.setVerticalScrollBarPolicy(vericalPolicy);
   background.add(hotpane);
  hotpane.setVisible(false);

       colddrink=new JPanel(new GridLayout((int)Math.ceil((double)BakeryController.countmain("colddrink") /3),3));
    colddrink.setBounds(135, 107, 500, 450);   
   coldpane = new JScrollPane(colddrink);
   coldpane.setBounds(135, 107, 500, 450);
   coldpane.setHorizontalScrollBarPolicy(horizontalPolicy);
   coldpane.setVerticalScrollBarPolicy(vericalPolicy);
   background.add(coldpane);
  coldpane.setVisible(false);
    
		
                
                
    JSeparator j= new JSeparator(SwingConstants.HORIZONTAL);
    j.setBounds(635, 450, 638, 400);
    j.setForeground(Color.red);
   background.add(j);
    total=new JLabel("Total Price:");
    total.setBounds(635, 470, 200, 100);
    total.setForeground(Color.WHITE);
    total.setBackground(Color.red);
    total.setFont(new Font("Time New Roman", Font.BOLD, 10));
    background.add(total);
    confrim=new JButton("CONFIRM");
    confrim.setBounds(635, 470, 100, 40);
    confrim.addActionListener(this);
  background.add(confrim);
    clear=new JButton("CLEAR");
    clear.setBounds(635, 530, 100, 40);
    clear.addActionListener(this);
  background.add(clear);
     
    receipt=new receiptpane();
    receipt.setBounds(635, 107, 280, 350);
  receipt.setBackground(Color.BLACK);
  
  background.  add(receipt);
   if(condition.equals("Bread"))
   {
       foodpane.setVisible(true);
   }else if(condition.equals("Cake"))
   {
       cakepane.setVisible(true);
   }else if(condition.equals("Hot"))
   {
       hotpane.setVisible(true);
   }else if(condition.equals("Cold"))
   {
      
       coldpane.setVisible(true);
   }


    ResultSet hotrs = BakeryController.IMAGESELECTOR("hotdrink");
    JButton[] hotarray = new JButton[BakeryController.QUANITITYIMAGESELECTOR("hotdrink")];
  
    ResultSet coldrs = BakeryController.IMAGESELECTOR("colddrink");
    JButton[] array = new JButton[BakeryController.QUANITITYIMAGESELECTOR("colddrink")];
    
    ResultSet cakedrs = BakeryController.IMAGESELECTOR("cakes");
    JButton[] cakearray = new JButton[BakeryController.QUANITITYIMAGESELECTOR("cakes")];
    
        ResultSet breadrs = BakeryController.IMAGESELECTOR("bread");
    JButton[] breadarray = new JButton[BakeryController.QUANITITYIMAGESELECTOR("bread")];

    
    int i=0;
    String c = "";
    ActionListener cold = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() instanceof JButton) {
                String text = ((JButton) e.getSource()).getText();
                if(c!= text){
                     String c = text;
                     dbcount1 = 0;
                     cakecount5 = 1;
                     caket5=500;
                     cakev5=2;
                     cakef5=1;
                }
                int g = BakeryController.selectderby(text);
              
                    
                    
          try {
              dbcount1++;
              ResultSet rs= BakeryController.namegetter(text);
             
              while(rs.next())
              {
                  price=rs.getString(2);
                 
              }
            
          } catch (SQLException ex) {
              ex.printStackTrace();
              ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
          } catch (ClassNotFoundException ex) {
              Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
          }
                    System.out.println(BakeryController.selectderby(text));
                   
      if(g==0) {
              receiptpane.addRow(text,"1",price);
              cakecount5++;
               total.setText("Total Price: " + receiptpane.Total());
               receiptpane.insertData(text,"1",price);
          }else{
      
              caket5=Integer.parseInt(price)*BakeryController.selectcountderby(text);
              
				cakev5++;
				cakef5++;
                                    System.out.println("A "+BakeryController.selectcountderby(text));
                                    int c = BakeryController.selectcountderby(text);
                                    c++;
				String amount=caket5+"";
				String quantity=c+"";
                                receiptpane.countcolumn(quantity, amount,text);
                                 receiptpane.updateData(text,quantity,amount);

              cakecount5++;
              total.setText("Total Price: " + receiptpane.Total());
      
              
          }
            revalidate();
              repaint();
                
            }
        }
    };
    while(coldrs.next()){
      // System.out.println(coldrs.getString("name"));
      
        JPanel item = new JPanel();
    item.setPreferredSize(new Dimension(166,150));
    
    BufferedImage im = ImageIO.read(coldrs.getBinaryStream("Image"));
    im =linearResizeBi(im, 168, 125);
     ImageIcon mats= new ImageIcon(im);
     JLabel ima = new JLabel(mats);
              //  JLabel Label10 = new JLabel("GReen");
		ima.setHorizontalAlignment(SwingConstants.CENTER);
		ima.setOpaque(true);
		ima.setBackground(Color.RED);
                ima.setPreferredSize(new Dimension(166,125));
	//	Label10.setBounds(330, 300, 166, 125);
                item.add(ima,BorderLayout.NORTH);
                 array[i] = new JButton(coldrs.getString("name"));
                 array[i].setPreferredSize(new Dimension(166,15));
                
        array[i].addActionListener(cold);
        item.add(array[i],BorderLayout.SOUTH);
                
               
                  colddrink.add(item);
    }
    
 while(hotrs.next()){
    //   System.out.println(hotrs.getString("name"));
      
        JPanel item = new JPanel();
    item.setPreferredSize(new Dimension(166,150));
    
    BufferedImage im = ImageIO.read(hotrs.getBinaryStream("Image"));
    im =linearResizeBi(im, 168, 125);
     ImageIcon mats= new ImageIcon(im);
     JLabel ima = new JLabel(mats);
              //  JLabel Label10 = new JLabel("GReen");
		ima.setHorizontalAlignment(SwingConstants.CENTER);
		ima.setOpaque(true);
		ima.setBackground(Color.RED);
                ima.setPreferredSize(new Dimension(166,125));
	//	Label10.setBounds(330, 300, 166, 125);
                item.add(ima,BorderLayout.NORTH);
                 hotarray[i] = new JButton(hotrs.getString("name"));
                 hotarray[i].setPreferredSize(new Dimension(166,15));
              
        hotarray[i].addActionListener(cold);
        item.add(hotarray[i],BorderLayout.SOUTH);
                
               
                  hotdrink.add(item);
    }
    
  while(cakedrs.next()){
     //  System.out.println(cakedrs.getString("name"));
      
        JPanel item = new JPanel();
    item.setPreferredSize(new Dimension(166,150));
    
    BufferedImage im = ImageIO.read(cakedrs.getBinaryStream("Image"));
    im =linearResizeBi(im, 168, 125);
     ImageIcon mats= new ImageIcon(im);
     JLabel ima = new JLabel(mats);
              //  JLabel Label10 = new JLabel("GReen");
		ima.setHorizontalAlignment(SwingConstants.CENTER);
		ima.setOpaque(true);
		ima.setBackground(Color.RED);
                ima.setPreferredSize(new Dimension(166,125));
	//	Label10.setBounds(330, 300, 166, 125);
                item.add(ima,BorderLayout.NORTH);
                 cakearray[i] = new JButton(cakedrs.getString("name"));
                 cakearray[i].setPreferredSize(new Dimension(166,15));
              
        cakearray[i].addActionListener(cold);
        item.add(cakearray[i],BorderLayout.SOUTH);
                
               
                  cakepanel.add(item);
    }
   while(breadrs.next()){
       System.out.println(breadrs.getString("name"));
      
        JPanel item = new JPanel();
    item.setPreferredSize(new Dimension(166,150));
    
    BufferedImage im = ImageIO.read(breadrs.getBinaryStream("Image"));
    im =linearResizeBi(im, 168, 125);
     ImageIcon mats= new ImageIcon(im);
     JLabel ima = new JLabel(mats);
              //  JLabel Label10 = new JLabel("GReen");
		ima.setHorizontalAlignment(SwingConstants.CENTER);
		ima.setOpaque(true);
		ima.setBackground(Color.RED);
                ima.setPreferredSize(new Dimension(166,125));
	//	Label10.setBounds(330, 300, 166, 125);
                item.add(ima,BorderLayout.NORTH);
                 breadarray[i] = new JButton(breadrs.getString("name"));
                 breadarray[i].setPreferredSize(new Dimension(166,15));
                
        breadarray[i].addActionListener(cold);
        item.add(breadarray[i],BorderLayout.SOUTH);
                
     
               
                  food.add(item);
    }
    
//      while(breadrs.next()){
//       System.out.println(breadrs.getString("name"));
//      
//        JPanel item = new JPanel();
//    item.setPreferredSize(new Dimension(166,150));
//    
//    BufferedImage im = ImageIO.read(breadrs.getBinaryStream("Image"));
//    im =linearResizeBi(im, 168, 125);
//     ImageIcon mats= new ImageIcon(im);
//     JLabel ima = new JLabel(mats);
//              //  JLabel Label10 = new JLabel("GReen");
//		ima.setHorizontalAlignment(SwingConstants.CENTER);
//		ima.setOpaque(true);
//		ima.setBackground(Color.RED);
//                ima.setPreferredSize(new Dimension(166,125));
//	//	Label10.setBounds(330, 300, 166, 125);
//                item.add(ima,BorderLayout.NORTH);
//                 breadarray[i] = new JButton(breadrs.getString("name"));
//                 breadarray[i].setPreferredSize(new Dimension(166,15));
//                  if(BakeryController.quantitygetter(cakedrs.getString("name"))<1)
//            {
//                 breadarray[i].setEnabled(false);
//                breadarray[i].setText("Sold Out");
//            }
//        breadarray[i].addActionListener(cold);
//        item.add(breadarray[i],BorderLayout.SOUTH);
//                
//               
//                  food.add(item);
//    }


    
 
 revalidate();
                repaint();
 
    
}
 static public BufferedImage linearResizeBi(BufferedImage origin, int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height ,BufferedImage.TYPE_INT_RGB);
        Graphics2D g = resizedImage.createGraphics();
        float xScale = (float)width / origin.getWidth();
        float yScale = (float)height / origin.getHeight();
        AffineTransform at = AffineTransform.getScaleInstance(xScale,yScale);
        g.drawRenderedImage(origin,at);
        g.dispose();
        return resizedImage;
    }   
        
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
      if(e.getSource()== back){
        
          
          try {
             
              removeAll();
              repaint();
              revalidate();
             
              JPanel a= new MainPanel();
             
              a.setBounds(0, 0, 900  , 600);
             
              add(a);
              
              
              
              
              repaint();
              validate();
          } catch (IOException ex) {
              Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
          } catch (FontFormatException ex) {
              Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
          }
          //espresso==mochacinno//irishcoffee==flatwhite
        
                  
      }
      
      
     
      
      else if(e.getSource()== bread)
      {   hotpane.setVisible(false);
            coldpane.setVisible(false);
          cakepane.setVisible(false);
          foodpane.setVisible(true);
           repaint();
              validate();
      }
      else if(e.getSource()== cake)
      {
          
            hotpane.setVisible(false);
            coldpane.setVisible(false);
             foodpane.setVisible(false);
            cakepane.setVisible(true);
              
              
              
              
              repaint();
              validate();
      }
      else if(e.getSource() == hot)
      {
          
            coldpane.setVisible(false);
             foodpane.setVisible(false);
            cakepane.setVisible(false);
               hotpane.setVisible(true);
              
              
              
              repaint();
              validate();
      }
      else if(e.getSource() == cold)
      {
         
             foodpane.setVisible(false);
            cakepane.setVisible(false);
               hotpane.setVisible(false);
               coldpane.setVisible(true);
              
              
              repaint();
              validate();
      }
      else if(e.getSource()== confrim)
      {
           int dialogButton = JOptionPane.YES_NO_OPTION;
           
        int reply=  JOptionPane.showConfirmDialog(this, "Are You Going to Order?","Warning",dialogButton);
          if(reply== JOptionPane.YES_OPTION)
          {
             
              Component comp = SwingUtilities.getRoot(this);
   ((Window) comp).dispose();
          new Vouncher();  
           
          count=1;
          reset();
          
         
            try {
                JFrame ma=new Main();
                
                SwingUtilities.getWindowAncestor(this).setVisible(false);
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        ma.setVisible(true);
                        
                    }
                });
             
          } catch (IOException ex) {
            
            } catch (WriterException ex) {
        
            } catch (FontFormatException ex) {
           
            }  catch (UnsupportedLookAndFeelException ex) {
                   Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
               }
        
         
          
          }
          else{
            JOptionPane.showMessageDialog(null, "OK");
          }
          
      }
      else if(e.getSource()== smith)
      {if(BakeryController.quantitygetter("Smith island cake")!= dbcount35){
          try {
              dbcount35++;
              ResultSet rs= BakeryController.namegetter("Smith island cake");
             
              while(rs.next())
              {
                  price=rs.getString(2);
                 
              }
            
          } catch (SQLException ex) {
              ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
          } catch (ClassNotFoundException ex) {
              Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
          }
          if(count==1) {
              receiptpane.addRow("Smith island cake","1",price);
              count++;
               total.setText("Total Price: " + receiptpane.Total());
               receiptpane.insertData("Smith island cake","1",price);
          }else{
              t=Integer.parseInt(price)*v;
				v++;
				f++;
				String amount=t+"";
				String quantity=f+"";
                                receiptpane.countcolumn(quantity, amount,"Smith island cake");
                                 receiptpane.updateData("Smith island cake",quantity,amount);

              count++;
              total.setText("Total Price: " + receiptpane.Total());
        
              
          }
      }else{
          smith.setEnabled(false);
          smith.setText("Sold Out");
      }
              revalidate();
              repaint();
          
      }
      else if(e.getSource()== snowball){
          if(BakeryController.quantitygetter("Snow ball")!= dbcount34){
          try {
              dbcount34++;
              ResultSet rs= BakeryController.namegetter("Snow ball");
             
              while(rs.next())
              {
                  price=rs.getString(2);
                 
              }
            
          } catch (SQLException ex) {
              ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
          } catch (ClassNotFoundException ex) {
              Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
          }
          
          
          if(count1==1) {
					
                                        receiptpane.addRow("Snow ball", "1", price);
                                        count1++;
                                        receiptpane.insertData("Snow ball","1",price);
           total.setText("Total Price: " + receiptpane.Total());}
          else{
              t1=Integer.parseInt(price)*v1;
						v1++;
						f1++;
						String amount=t1+"";
						String quantity=f1+"";
                                                 receiptpane.countcolumn(quantity, amount,"Snow ball");
                                                 receiptpane.updateData("Snow ball",quantity,amount);

              count1++;
              total.setText("Total Price: " + receiptpane.Total());
          }
      }else{
              snowball.setEnabled(false);
              snowball.setText("Sold Out");
          }
 revalidate();          
repaint();
      }
      else if(e.getSource()==spice)
      {
               if(BakeryController.quantitygetter("Spice cake")!=dbcount33){
          try {
         dbcount33++;
              ResultSet rs= BakeryController.namegetter("Spice cake");
             
              while(rs.next())
              {
                  price=rs.getString(2);
                 
              }
            
          } catch (SQLException ex) {
              ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
          } catch (ClassNotFoundException ex) {
              Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
          }
          if(count3==1) {
              receiptpane.addRow("Spice cake", "1", price);
              count3++;
               total.setText("Total Price: " + receiptpane.Total());
                receiptpane.insertData("Spice cake","1",price);
          }
          else{
              t3=Integer.parseInt(price)*v3;
						v3++;
						f3++;
						String amount=t3+"";
						String quantity=f3+"";
                                                receiptpane.countcolumn(quantity, amount,"Spice cake");
                                                  receiptpane.updateData("Spice cake",quantity,amount);
                                                
              count3++;
              total.setText("Total Price: " + receiptpane.Total());
             
          }
               }else{
                   spice.setEnabled(false);
                   spice.setText("Sold Out");
               }
          revalidate();          
repaint();
      }else if(e.getSource()==clear)
      {
          receiptpane.clear();
         
          reset();
          revalidate();          
repaint();
				
      }
    }
 public static void reset(){
     
//     count=1;t=500;v=2;f=1;t1=500;v1=2;count1=1;f1=1;count3=1;t3=500;v3=2;f3=1;breadcount1=1;breadcount2=1;breadcount3=1;breadcount4=1;breadcount5=1;
//           breadcount6=1;breadcount7=1;breadcount8=1;breadcount9=1;breadt1=500;breadt2=500;breadt3=500;breadt4=500;breadt5=500;breadt6=500;breadt7=500;
//           breadt8=500;breadt9=500;breadf1=1;breadf2=1;breadf3=1;breadf4=1;breadf5=1;breadf6=1;breadf7=1;breadf8=1;breadf9=1;breadv1=2;breadv2=2;breadv3=2;
//           breadv4=2;breadv5=2;breadv6=2;breadv7=2;breadv8=2;breadv9=2;coldcount1=1;coldcount2=1;coldcount3=1;coldcount4=1;coldcount5=1;
//           coldcount6=1;coldcount7=1;coldcount8=1;coldcount9=1;coldt1=500;coldt2=500;coldt3=500;coldt4=500;coldt5=500;coldt6=500;coldt7=500;coldt8=500;
//           coldt9=500;coldv1=2;coldv2=2;coldv3=2;coldv4=2;coldv5=2;coldv6=2;coldv7=2;coldv8=2;coldv9=2;coldf1=1;coldf2=1;coldf3=1;coldf4=1;coldf5=1;coldf6=1;
//           coldf7=1;coldf8=1;coldf9=1;hotcount1=1;hotcount2=1;hotcount3=1;hotcount4=1;hotcount5=1;hotcount6=1;hotcount7=1;hotcount8=1;hotcount9=1;
//           hott1=500;hott2=500;hott3=500;hott4=500;hott5=500;hott6=500;hott7=500;hott8=500;hott9=500;hotv1=2;hotv2=2;hotv3=2;hotv4=2;hotv5=2;hotv6=2;hotv7=2;hotv8=2;hotv9=2;
//           hotf1=1;hotf2=1;hotf3=1;hotf4=1;hotf5=1;hotf6=1;hotf7=1;hotf8=1;hotf9=1;cakecount1=1;cakecount2=1;cakecount3=1;cakecount4=1;cakecount5=1;cakecount6=1;
//           caket1=500;caket2=500;caket3=500;caket4=500;caket5=500;caket6=500;cakev1=2;cakev2=2;cakev3=2;cakev4=2;cakev5=2;cakev6=2;cakef1=1;cakef2=1;cakef3=1;
//                   cakef4=1;cakef5=1;cakef6=1;
//                   dbcount=0;dbcount1=0;dbcount2=0;dbcount3=0;dbcount4=0;dbcount5=0;dbcount6=0;dbcount7=0;dbcount8=0;
//           dbcount9=0;dbcount10=0;dbcount11=0;dbcount12=0;dbcount13=0;dbcount14=0;dbcount15=0;dbcount16=0;dbcount17=0;
//           dbcount18=0;dbcount19=0;dbcount20=0;dbcount21=0;dbcount22=0;dbcount23=0;dbcount24=0;dbcount25=0;dbcount26=0;
//           dbcount27=0;dbcount28=0;dbcount29=0;dbcount30=0;dbcount31=0;dbcount32=0;dbcount33=0;dbcount34=0;dbcount35=0;dbcount36=0;
//           smith.setEnabled(true);snowball.setEnabled(true);spice.setEnabled(true);
//            tiramisu.setEnabled(true);redvel.setEnabled(true);camat.setEnabled(true);batten.setEnabled(true);cheesecake.setEnabled(true);saltencaramal.setEnabled(true);
//            callah.setEnabled(true);bangen.setEnabled(true);potatobread.setEnabled(true);beerbread.setEnabled(true);panetone.setEnabled(true);muntigranbread.setEnabled(true);bagueddeyeast.setEnabled(true);eggpie.setEnabled(true);crostata.setEnabled(true);
//            coffeeamericano.setEnabled(true);hotchocolate.setEnabled(true);cappucino.setEnabled(true);espresso.setEnabled(true);irishcoffee.setEnabled(true);lattecoffee.setEnabled(true);milk.setEnabled(true);eggnog.setEnabled(true);mochachino.setEnabled(true);
//            chocolate.setEnabled(true);banana.setEnabled(true);smoothie.setEnabled(true);icecoffee.setEnabled(true);lemonicetea.setEnabled(true);strawberrymilkshake.setEnabled(true);icedcappucino.setEnabled(true);coffeesoda.setEnabled(true);greentea.setEnabled(true);
//                               
//            smith.setText("Smith Island");snowball.setText("Snowball");spice.setText("Spice");
//            tiramisu.setText("Tiramisu");redvel.setText("Red Velvet");camat.setText("Carrot Cake");batten.setText("Batten Cake");cheesecake.setText("Cheese Cake");saltencaramal.setText("Caramel");
//            callah.setText("Challah");bangen.setText("Bangen");potatobread.setText("Potato Bread");beerbread.setText("Beer Bread");panetone.setText("Panettone");muntigranbread.setText("Multigrain");bagueddeyeast.setText("Baguedde");eggpie.setText("Egg Pie");crostata.setText("Crostata");
//            coffeeamericano.setText("Americano");hotchocolate.setText("Chocolate");cappucino.setText("Cappuccino");espresso.setText("Espresso");irishcoffee.setText("Irish Coffee");lattecoffee.setText("Latte");milk.setText("Milk");eggnog.setText("Egg Nog");mochachino.setText("Mochachino");
//            chocolate.setText("Chocolate");banana.setText("Chocolate Banana");smoothie.setText("Blueberry");icecoffee.setText("Iced Coffee");lemonicetea.setText("Lemon Tea");strawberrymilkshake.setText("Strawberry");icedcappucino.setText("Iced Cappuccino");coffeesoda.setText("Coffee Soda");greentea.setText("Green Tea");
            total.setText("Total Price:");
                                
     
 }
 
}
