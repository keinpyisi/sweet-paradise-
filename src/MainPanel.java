
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontFormatException;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krul
 */
public class MainPanel extends JPanel implements ActionListener{
    private JButton homebtn,contactbtn,aboutbtn,orderbtn,breadbtn,cakebtn,coldbtn,hotbtn;
    private JLabel main;
    public MainPanel() throws IOException, FontFormatException{
        setPreferredSize(new Dimension(900, 600));
        setLayout(null);
       
        
       //BufferedImage myImage = ImageIO.read(getClass().getResourceAsStream("img/menu.jpg"));
           ImageIcon icon= new ImageIcon(getClass().getResource("img/menu.jpg"));
           ImageIcon yuru= new ImageIcon(getClass().getResource("img/yuru.png"));
     JLabel background= new JLabel(icon);
     background.setHorizontalAlignment(SwingConstants.RIGHT);
     background.setBounds(0, 0, 900, 600);
     background.setOpaque(true);
     background.setBackground(Color.BLUE.darker());
     JLabel yurucamp= new JLabel(yuru);
     yurucamp.setBounds(580, 370, 337, 190);
     background.add(yurucamp);
        homebtn=new JButton("Home");
        homebtn.setBounds(50, 80, 80, 50);
        homebtn.addActionListener(this);
      background.add(homebtn);
       contactbtn=new JButton("Contact");
       contactbtn.setBounds(50, 180, 80, 50);
       contactbtn.addActionListener(this);
       background.add(contactbtn);
       aboutbtn=new JButton("About");
       aboutbtn.setBounds(50, 280, 80, 50);
       aboutbtn.addActionListener(this);
       background.add(aboutbtn);
       orderbtn=new JButton("Order");
       orderbtn.setBounds(50, 380, 80, 50);
       orderbtn.addActionListener(this);
       background.add(orderbtn);
        JSeparator jsps= new JSeparator(SwingConstants.VERTICAL);
    jsps.setBounds(190, 0, 900, 600);
    jsps.setForeground(Color.WHITE);
   background. add(jsps);
       main=new JLabel("Main Menu");
       main.setBounds(500, -20, 500, 200);
       main.setFont(BakeryController.acherus());
       main.setForeground(BakeryController.Blood_Red());
       background.add(main);
       breadbtn=new JButton("Bread");
       breadbtn.setBounds(400, 120, 130, 50);
       breadbtn.addActionListener(this);
       background.add(breadbtn);
       coldbtn=new JButton("Cold Drinks");
       coldbtn.setBounds(400, 220, 130, 50);
       coldbtn.addActionListener(this);
      background. add(coldbtn);
       cakebtn=new JButton("Cake");
       cakebtn.setBounds(650, 120, 130, 50);
       cakebtn.addActionListener(this);
     background.  add(cakebtn);
       hotbtn=new JButton("Hot Drinks");
        hotbtn.setBounds(650, 220, 130, 50);
       hotbtn.addActionListener(this);
     background.  add(hotbtn);
       add(background);
       
       
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()== homebtn){
           
           
          
            try {
                Component comp = SwingUtilities.getRoot(this);
   ((Window) comp).dispose();
                JFrame ma=new Main();
                
                SwingUtilities.getWindowAncestor(this).setVisible(false);
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        ma.setVisible(true);
                        
                    }
                }); } catch (Exception ex) {
               
            } 
         
            
            
        }
        else if(e.getSource()== breadbtn)
        {
            try {
                removeAll();
                
                setSize(new Dimension(900, 600));
                
                add(new Receipt("Bread"));
                this.revalidate();
                
                //re.setVisible(true);
                this.repaint();
            } catch (ClassNotFoundException ex) {
           //     
            } catch (SQLException ex) {
               ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
            } catch (FontFormatException ex) {
           
            } catch (IOException ex) {
           
            }
            
        }
        else if(e.getSource()== cakebtn)
        {
            try {
                removeAll();
                
                setSize(new Dimension(900, 600));
                add(new Receipt("Cake"));
                this.revalidate();
                
                //re.setVisible(true);
                this.repaint();
            } catch (Exception ex) {
                
            } 
               
        }
        else if(e.getSource()== hotbtn)
        {
            try {
                removeAll();
                
                setSize(new Dimension(900, 600));
                add(new Receipt("Hot"));
                this.revalidate();
                
                //re.setVisible(true);
                this.repaint();
            } catch (Exception ex) {
               
            } 
            
        }
        else if(e.getSource()== coldbtn){
            try {
                removeAll();
                
                setSize(new Dimension(900, 600));
                add(new Receipt("Cold"));
                this.revalidate();
                
                //re.setVisible(true);
                this.repaint();
            } catch (Exception ex) {
               
            } 
            
        }
        else if(e.getSource()==orderbtn){
            
            try {
                removeAll();
                
                setSize(new Dimension(900, 600));
                add(new Receipt("Bread"));
                this.revalidate();
                
                //re.setVisible(true);
                this.repaint();
            } catch (ClassNotFoundException ex) {
              //  
            } catch (SQLException ex) {
               // 
            } catch (FontFormatException ex) {
               // 
            } catch (IOException ex) {
               // 
            }
            
        }
        else if(e.getSource()== contactbtn)
        {
           String msg = "<html><font size='6' color='#A52A2A'>PH : 097833331 <br>Address : Pansodan <br> Email : sweetparadise@gmail.com</font>";

    JOptionPane optionPane = new WideOptionPane();
    optionPane.setMessage(msg);
    optionPane.setMessageType(JOptionPane.INFORMATION_MESSAGE);
    JDialog dialog = optionPane.createDialog(null, "Contact Me");
    dialog.setVisible(true);
        }
        else if(e.getSource()== aboutbtn)
        {
            removeAll();
            setSize(900, 600);
            add(new About());
            this.revalidate();
            this.repaint();
        }
    }
    
}
class WideOptionPane extends JOptionPane {

  WideOptionPane() {
  }

  public int getMaxCharactersPerLineCount() {
    return 1000;
  }
}
