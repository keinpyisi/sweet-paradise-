
import com.google.zxing.WriterException;
import com.jtattoo.plaf.hifi.HiFiLookAndFeel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Executor;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krul
 */
public class Main extends JFrame implements ActionListener{

    /**
     * @param args the command line arguments
     */
    
   private JPanel main;
     private JLabel title,sub,sub2,img;
    private JButton enter,inv;
    public static void main(String[] args) {
       try {
           // TODO code application logic here
           System.err.println("???");
           UIManager.setLookAndFeel(new HiFiLookAndFeel());
           new Main();
       } catch (Exception ex) {
          // Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
          ex.printStackTrace();
       } 
    }
    
    public Main() throws IOException, WriterException, FontFormatException, UnsupportedLookAndFeelException{
       
       // UIManager.setLookAndFeel(new HiFiLookAndFeel());
         URL iconurl= getClass().getResource("img/icon.PNG");
              Toolkit tk= this.getToolkit();
               Image ima= tk.getImage(iconurl);
               setIconImage(ima);
               
         String uniqueID = UUID.randomUUID().toString();
          
            Vouncher.id(uniqueID);
           
            Vouncher.data(BakeryController.getQRCodeImage(uniqueID, 100, 100));
           
           
        //  setContentPane(new JLabel(icon));
        
        main=new JPanel(null);
       
        //main.setBackground(Color.red);
        //main.setOpaque(!isOpaque());
        
        JLabel la= new JLabel(new ImageIcon ( getClass().getResource("img/main.jpg")));
          
        la.setBounds(0, 00, 400, 400);
       main.add(la);
       
       img=new JLabel(new ImageIcon ( getClass().getResource("img/icon.PNG")));
         //img.setBackground(Color.red);
       
            img.setBounds(170, 10, 50, 50);
  //           System.err.println("YO");
            la.add(img);
    //        System.err.println("YO");
        main.setPreferredSize(new Dimension(400,400));
      //  main.setOpaque(true);
       // main.setBackground(Color.red);
        setSize(new Dimension(400,400));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
           
       
      
        setLayout(null);
        main.setBounds(0, 0, 400, 400);
        title=new JLabel("Sweet Paradise");
        title.setBounds(120, 20, 200, 100);
        title.setFont(BakeryController.Bellota());
        title.setForeground(Color.WHITE);
      la.add(title);
        sub=new JLabel("💙💙💙");
        sub.setForeground(Color.red);
        sub.setFont(BakeryController.Emoji());
        sub.setBounds(150, 50, 100, 100);
        la.add(sub);
        
        sub2=new JLabel("いらしゃいませ");
        sub2.setFont(new Font("Time New Roman", Font.PLAIN, 20));
        sub2.setForeground(Color.WHITE);
       
       
        sub2.setBounds(120, 80, 200, 100);
     la.add(sub2);
        enter=new JButton("ENTER");
        enter.setBounds(270, 300, 100, 50);
      //  enter.setFont(BakeryController.acherus());
        enter.addActionListener(this);
       la.add(enter);
        inv=new JButton("Inventory");
        inv.setBounds(20, 300, 100, 50);
        inv.addActionListener(this);
       la.add(inv);
       
       setResizable(false);
     
        
      
        
       add(main); 
       main.repaint();
       Executor executoras=java.util.concurrent.Executors.newSingleThreadExecutor();
       executoras.execute(new Runnable() {
           
            @Override
            public void run() {
               // main.setVisible(true);
             setVisible(true);
            }
        });
      
          
        
    }
    
public void actionPerformed(ActionEvent e) {
        if(e.getSource()== enter)
        {   try {
           
            //  new Vouncher();
            receiptpane.clear();
         BakeryController.drop();
           
            main.setVisible(false);
             setSize(new Dimension(900, 600));
             Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
           
            JPanel enterpanel=new MainPanel();
            enterpanel.setBounds(0, 0, 900, 600);
            add(enterpanel);
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                     BakeryController.createTable();
                    enterpanel.setVisible(true);
                }
            }); } catch (Exception ex) {
                
            } 
          
            
            
        }
        else if(e.getSource()== inv){
            
                main.setVisible(false);
                setVisible(false);
                //setSize(new Dimension(800, 600));
                new Login();
            
            
        }
   
       
    }

}
    

