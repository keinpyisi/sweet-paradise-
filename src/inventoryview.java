
import com.google.zxing.WriterException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krul
 */
public class inventoryview extends JFrame implements MouseListener{

    /**
     * @param args the command line arguments
     */
    static JLabel title,mini,back,sub,arrow,background;
    static JPanel table,table1;
    private static JFrame frame;
    public static void main(String[] args) {
        try {
            new inventoryview();
        } catch (SQLException ex) {
              ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
        } catch (ClassNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } catch (FontFormatException ex) {
            
        }
    }
    
    public inventoryview() throws SQLException, ClassNotFoundException, IOException, FontFormatException, FontFormatException{
         URL iconurl= getClass().getResource("img/icon.PNG");
              Toolkit tk= this.getToolkit();
               Image ima= tk.getImage(iconurl);
               setIconImage(ima);
frame=new JFrame();

        frame.setSize(new Dimension(700, 600));
        //setDefaultCloseOperation(EXIT_ON_CLOSE);
       // setLocationRelativeTo(null);
       
        BufferedImage myImage = ImageIO.read(getClass().getResourceAsStream("img/inv4.jpg"));
           ImageIcon icon= new ImageIcon(myImage);
      background= new JLabel(icon);
     background.setBounds(0, 0, 700, 600);
        frame.setLayout(null);
        title=new JLabel("Sweet Paradise");
        title.setFont(new Font("Time New Roman", Font.BOLD, 25));
        title.setForeground(BakeryController.Sky_blue());
        title.setBounds(20, -20, 200, 100);
      background.add(title);
        mini=new JLabel("Bakery & Cafe");
        mini.setFont(new Font("Time New Roman", Font.PLAIN, 20));
        mini.setBounds(40, 30, 170, 100);
        mini.setForeground(BakeryController.Sky_blue());
        background.add(mini);
        JSeparator j= new JSeparator(SwingConstants.HORIZONTAL);
    j.setBounds(0, 100, 738, 400);
    j.setForeground(Color.red);
    background.add(j);
    sub=new JLabel("Here are Things until Things");
    sub.setFont(new Font("Time New Roman", Font.BOLD, 20));
    sub.setBounds(350, 20, 350, 100);
    sub.setForeground(BakeryController.Rose_Gold());
    background.add(sub);
    ImageIcon icons= new ImageIcon(getClass().getResource("img/arrows.png"));
    arrow=new JLabel(icons);
    arrow.setBounds(640, 10, 50, 50);
    arrow.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           new Cal();
            }

            @Override
            public void mousePressed(MouseEvent e) {
        //        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseReleased(MouseEvent e) {
       //         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseEntered(MouseEvent e) {
      //          throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseExited(MouseEvent e) {
        //        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    background.add(arrow);
    back=new JLabel("Inventory");
    back.setFont(new Font("Time New Roman", Font.BOLD, 16));
    back.setBounds(10, 100, 300, 100);
   // back.addMouseListener(this);
    back.setForeground(Color.WHITE);
    background.add(back);
    
    
    
    JLabel right=new JLabel("Ingredients");
    right.setFont(new Font("Time New Roman", Font.BOLD, 16));
    right.setBounds(450, 100, 300, 100);
    right.setForeground(Color.WHITE);
    background.add(right);
//    table=new ingredientpanel();
//   // table.setBackground(Color.red);
//    table.setBounds(0, 180, 350, 400);
//    table.setOpaque(!frame.isOpaque());
//    background.add(table);
//    table1=new InventoryPanel();
//    //table1.setBackground(Color.BLACK.brighter());
//    table1.setBounds(450, 180, 250, 370);
//  //  table1.setOpaque(!isOpaque());
//    background.add(table1);
   frame. add(background);
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
         //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowClosing(WindowEvent e) {
                try {
                  frame.  setVisible(false);
                    JFrame ma=new Main();
                    //setVisible(false);
//           SwingUtilities.getWindowAncestor(this).setVisible(false);
java.awt.EventQueue.invokeLater(new Runnable() {
    public void run() {
        ma.setVisible(true);
    }
});     } catch (Exception ex) {
                    
                } 
            }

            @Override
            public void windowClosed(WindowEvent e) {
                try {
                    frame.setVisible(false);
                    JFrame ma=new Main();
                    //setVisible(false);
//           SwingUtilities.getWindowAncestor(this).setVisible(false);
java.awt.EventQueue.invokeLater(new Runnable() {
    public void run() {
        ma.setVisible(true);
    }
});     } catch (Exception ex) {
                   
                } 
            }

            @Override
            public void windowIconified(WindowEvent e) {
          //      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
       //         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowActivated(WindowEvent e) {
       //         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
       //         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
       frame. repaint();
       frame. revalidate();
      frame.  setResizable(false);
       frame. setVisible(true);
         String currentDateTimeString;
      
        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());   
       frame. setTitle("SweetParadise Administration: " + currentDateTimeString);
       
        
    }
    public static void redraw(){
        
        
        try {
            background.remove(table);
            background.remove(table1);
            table=new ingredientpanel();
            // table.setBackground(Color.red);
            table.setBounds(0, 180, 350, 400);
            table.setOpaque(!frame.isOpaque());
            background.add(table);
            table1=new InventoryPanel();
            //table1.setBackground(Color.BLACK.brighter());
            table1.setBounds(450, 180, 250, 400);
            //  table1.setOpaque(!isOpaque());
            background.add(table1);
            table.repaint();
            table.revalidate();
            table1.repaint();
            table1.revalidate();
            background.repaint();
            background.revalidate();
            frame. repaint();
            frame. revalidate();
            
            
            
            System.err.println("Frame Checked");
        } catch (ClassNotFoundException ex) {
           // Logger.getLogger(inventoryview.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
        //    Logger.getLogger(inventoryview.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
         //   Logger.getLogger(inventoryview.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FontFormatException ex) {
         //   Logger.getLogger(inventoryview.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }

    

    @Override
    public void mouseClicked(MouseEvent e) {
  //      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  if(e.getSource()== back){
      try {
frame.          setVisible(false);
          JFrame ma=new Main();
          //setVisible(false);
//           SwingUtilities.getWindowAncestor(this).setVisible(false);
java.awt.EventQueue.invokeLater(new Runnable() {
    public void run() {
        ma.setVisible(true);
    }
});
      } catch (Exception ex) {
          
      } 
          
           
  }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
    
}
