
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krul
 */
public class Login extends JFrame implements ActionListener{

    /**
     * @param args the command line arguments
     */
    private JLabel title,sub,to,name,pass;
    private JTextField username;
    private JPasswordField password;
    private JButton back,login;
    private JFrame invpanel;
    public static void main(String[] args) {
        // TODO code application logic here
        new Login();
    }
public Login(){
    setSize(new Dimension(450,350));
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setLayout(null);
    title=new JLabel("Sweet Paradise");
    title.setBounds(150, -20, 200, 100);
    title.setFont(new Font("Time New Roman",Font.PLAIN,20));
    add(title);
    sub=new JLabel("Bakery & Cafe");
    sub.setBounds(165, 20, 200, 100);
    sub.setFont(new Font("Time New Roman", Font.PLAIN, 15));
    add(sub);
     JSeparator j= new JSeparator(SwingConstants.HORIZONTAL);
    j.setBounds(0, 100, 738, 400);
    j.setForeground(Color.red);
    add(j);
    to=new JLabel("To Inventory");
    to.setBounds(180, 80, 200, 100);
    add(to);
    name=new JLabel("Username");
    name.setBounds(100, 120, 200, 100);
    add(name);
    username=new JTextField();
    username.setBounds(190, 160, 200, 20);
    add(username);
    pass=new JLabel("Password");
    pass.setBounds(100, 180, 200, 100);
    add(pass);
    password=new JPasswordField();
    password.setBounds(190, 215, 200, 20);
    add(password);
   
   
    back=new JButton("Back");
    back.setBounds(100, 265, 100, 40);
   
    back.addActionListener(this);
    add(back);
    
    login=new JButton("Login");
    login.setBounds(250, 265, 100, 40);
  
     login.addActionListener(this);
     setResizable(false);
    add(login);
    setVisible(true);
    
}
    @Override
    public void actionPerformed(ActionEvent e) {
      if(e.getSource()==login){
          try {
           
              if(username.getText().isEmpty() || password.getPassword().length==0){
                  JOptionPane.showMessageDialog(null, "Are You Admin? Wrong Password or Username");
                 
              }else
              {
                  String[] admin=null;
                  ResultSet rs= BakeryController.admin(username.getText());
                  
                  while (rs.next())
                  {
                      admin=new String[]{rs.getString("username") , rs.getString("password")};
                  }


                  char[]passwordtxt=password.getPassword();
                  char[]user=admin[1].toCharArray();
                  
                  if(admin[0].equals(username.getText())&& Arrays.equals(passwordtxt, user))
                  {
                  
                  // invpanel= new inventoryview();
                       new Cal();
              setVisible(false);
                  }
                  else{
                        JOptionPane.showMessageDialog(null, "Are You Admin? Wrong Password or Username");
                      
                  }
              

              }
              } catch (SQLException ex) {
             ImageIcon icon = new ImageIcon(Loading.class.getResource("img/errorlogo.png"));
                   //  JOptionPane.showMessageDialog(null, "Con","Con",JOptionPane.ERROR_MESSAGE);
                   JOptionPane.showMessageDialog(
                           null,
                           
                           "Please Check your Connection!!","Error Connecting", JOptionPane.ERROR_MESSAGE,
                           icon);
                   System.exit(0);
          } catch (ClassNotFoundException ex) {
         
          }catch(NullPointerException ex)
          {
         //     JOptionPane.showMessageDialog(null, "Are You Admin? Wrong Password or Username");
               Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
          }
           } 
      
      else if(e.getSource()== back){
          try {
              setVisible(false);
              JFrame ma=new Main();
                  
java.awt.EventQueue.invokeLater(new Runnable() {
    public void run() {
        ma.setVisible(true);
    }
});
          } catch (Exception ex) {
             
          } 
      }
    }
    
}
